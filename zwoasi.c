#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "ASICamera2.h"
#include "zwoasi.h"
#include "fits.h"

/* This is the interface to the camera's library.
 * Developed with version 1.17 of the SDK.
 * Warning: most functions do not return errors, they just call exit() on error.
 */

char* ASI_bayer_patterns[] = {"RGGB", "BGGR", "GRBG", "GBRG"};

void open_camera(int check_guiding) {
	int num_devices = ASIGetNumOfConnectedCameras();
	if (num_devices <= 0) {
		fprintf(stderr, "no camera found\n");
		exit(1);
	}

	ASI_CAMERA_INFO camera_info;
	ASIGetCameraProperty(&camera_info, 0);

	if (check_guiding && !camera_info.ST4Port) {
		fprintf(stderr, "camera %s does not support guiding, aborting\n", camera_info.Name);
		exit(1);
	}
	if (ASIOpenCamera(0) != ASI_SUCCESS) {
		fprintf(stderr, "could not open camera %s\n", camera_info.Name);
		exit(1);
	}
	if (ASI_SUCCESS != ASIInitCamera(0)) {
		fprintf(stderr, "could not init camera %s\n", camera_info.Name);
		ASICloseCamera(0);
		exit(1);
	}
	if (!camera_info.IsUSB3Host || !camera_info.IsUSB3Camera)
		fprintf(stderr, "USB3 is not enabled or supported\n");
	fprintf(stdout, "found camera %s\n", camera_info.Name);

	//if (camera_info.IsColorCam == ASI_TRUE)
	//	fprintf(stdout, "Color Camera, bayer pattern: %s\n", ASI_bayer_patterns[camera_info.BayerPattern]);
	fprintf(stdout, "image size: %ldx%ld\n", camera_info.MaxWidth, camera_info.MaxHeight);
}

/* transition function to return values instead of exit(2) */
camera_capabilities *get_camera_info() {
	int num_devices = ASIGetNumOfConnectedCameras();
	if (num_devices <= 0) {
		fprintf(stderr, "no camera found\n");
		return NULL;
	}

	camera_capabilities *retval = calloc(1, sizeof(camera_capabilities));
	ASI_CAMERA_INFO camera_info;
	ASIGetCameraProperty(&camera_info, 0);

	retval->identifier = 0;
	retval->name = strdup(camera_info.Name);
	retval->is_osc = camera_info.IsColorCam;
	retval->has_guiding_port = camera_info.ST4Port;

	if (ASIOpenCamera(0) != ASI_SUCCESS)
		retval->is_open = 0;
	else retval->is_open = 1;

	if (ASI_SUCCESS != ASIInitCamera(0)) {
		ASICloseCamera(0);
		retval->is_open = 0;
		return retval;
	}
	if (!camera_info.IsUSB3Host || !camera_info.IsUSB3Camera) {
		ASICloseCamera(0);
		retval->is_open = 0;
	}
	return retval;
}

static void apply_exposure_params(exposure_params params, ASI_CAMERA_INFO *camera_info, int *rx, int *ry) {

	if (ASI_SUCCESS != ASISetControlValue(0, ASI_GAIN, params.gain, ASI_FALSE)) {
		fprintf(stderr, "failed to set gain %d\n", params.gain);
		ASICloseCamera(0);
		exit(1);
	}

	ASIGetCameraProperty(camera_info, 0);
	fprintf(stdout, "camera ADC used: %d bits\n", camera_info->BitDepth);

	if (ASI_SUCCESS != ASISetControlValue(0, ASI_EXPOSURE, params.exposure_ms*1000, ASI_FALSE)) {
		fprintf(stderr, "failed to set exposure %d ms\n", params.exposure_ms);
		ASICloseCamera(0);
		exit(1);
	}

	if (ASI_SUCCESS != ASISetControlValue(0, ASI_BRIGHTNESS, params.offset, ASI_FALSE)) {
		fprintf(stderr, "failed to set offset %d\n", params.offset);
		ASICloseCamera(0);
		exit(1);
	}

	if (camera_info->IsColorCam == ASI_TRUE && params.bin != 1)
		fprintf(stderr, "Warning: using binning on a color camera is not recommended\n");

	int w = camera_info->MaxWidth / params.bin;
	int h = camera_info->MaxHeight / params.bin;
	if (ASI_SUCCESS != ASISetROIFormat(0, w, h, params.bin, ASI_IMG_RAW16)) {
		fprintf(stderr, "failed to set image format (binning %d, 16 bits)\n", params.bin);
		ASICloseCamera(0);
		exit(1);
	}
	if (rx) *rx = w;
	if (ry) *ry = h;

	/* list available controls for the camera (disabled for speed) */
	int nb_controls = 0;
	if (ASI_SUCCESS != ASIGetNumOfControls(0, &nb_controls)) {
		fprintf(stderr, "failed to get the number of camera controls\n");
		ASICloseCamera(0);
		exit(1);
	}
	for(int i = 0; i < nb_controls; i++) {
		ASI_CONTROL_CAPS caps;
		ASIGetControlCaps(0, i, &caps);
		ASI_BOOL cur_auto;
		long cur_val;
		ASIGetControlValue(0, caps.ControlType, &cur_val, &cur_auto);
		fprintf(stdout, "control %s: %ld%s\t%s [%ld, %ld] default=%ld %s%s\n",
				caps.Name, cur_val, cur_auto == ASI_TRUE ? " (auto)" : "",
				caps.Description, caps.MinValue, caps.MaxValue,
				caps.DefaultValue,
				caps.IsAutoSupported == ASI_TRUE ? "A" : "",
				caps.IsWritable == ASI_TRUE ? "W" : "");
	}

	if (camera_info->IsTriggerCam) {
		ASI_SUPPORTED_MODE mode;
		ASIGetCameraSupportMode(0, &mode);
		for (int i = 0; i < 16; i++) {
			if (mode.SupportedCameraMode[i] == ASI_MODE_END)
				break;
			fprintf(stdout, "supported trigger mode: %d\n", mode.SupportedCameraMode[i]);
		}
		if (ASI_SUCCESS != ASISetCameraMode(0, ASI_MODE_NORMAL)) {
			fprintf(stderr, "could not set normal capture mode\n");
			ASICloseCamera(0);
			exit(1);
		}
	}
}

/* if filename is not NULL, the image is saved as FITS
 * if image is not NULL, the image data is returned as float, only for green if camera is color
 */
void capture_frame(exposure_params params, const char *filename, image *image) {
	int w, h;
	ASI_CAMERA_INFO camera_info;
	apply_exposure_params(params, &camera_info, &w, &h);

	ASISetControlValue(0, ASI_BANDWIDTHOVERLOAD, 70, ASI_FALSE);
	ASISetControlValue(0, ASI_HIGH_SPEED_MODE, 1, ASI_FALSE);

	long temp;
	ASI_BOOL bAuto = ASI_FALSE;
	if (ASI_SUCCESS != ASIGetControlValue(0, ASI_HIGH_SPEED_MODE, &temp, &bAuto)) {
		fprintf(stderr, "could not retrieve speed mode\n");
	}
	fprintf(stdout, "HIGH SPEED MODE: %ld\n", temp);

	//long temp;
	//ASI_BOOL bAuto = ASI_FALSE;
	if (ASI_SUCCESS != ASIGetControlValue(0, ASI_TEMPERATURE, &temp, &bAuto)) {
		fprintf(stderr, "could not retrieve sensor temperature\n");
		temp = -10000;
	}

	size_t buffer_size = w * h * 2;
	unsigned char* buffer = malloc(buffer_size);
	if (!buffer) {
		fprintf(stderr, "memory allocation failed\n");
		ASICloseCamera(0);
		exit(1);
	}

	fprintf(stdout, "starting exposure of %g second(s)\n", params.exposure_ms/1000.0);
	char *start_time = get_time();

	ASIStopVideoCapture(0);
	ASI_ERROR_CODE status;
	ASI_EXPOSURE_STATUS exp_status;
	if (ASI_SUCCESS != (status = ASIStartExposure(0, ASI_FALSE))) {
		free(buffer);
		fprintf(stderr, "failed to start exposure\n");
		ASICloseCamera(0);
		exit(1);
	}
	usleep(params.exposure_ms * 1000);
	do {
		usleep(10000); //10ms
		ASIGetExpStatus(0, &exp_status);
		//fprintf(stdout, "exposure status: %d\n", exp_status);
	} while (exp_status == ASI_EXP_WORKING);

	if (exp_status == ASI_EXP_SUCCESS) {
		if (ASI_SUCCESS != (status = ASIGetDataAfterExp(0, buffer, buffer_size))) {
			free(buffer);
			fprintf(stderr, "failed to get image data (status: %d\n)", status);
			ASICloseCamera(0);
			exit(1);
		}
		ASIStopExposure(0);
	}
	else {
		free(buffer);
		fprintf(stderr, "could not get the exposure (status: %d)\n", exp_status);
		exit(1);
	}

	long temp2;
	if (ASI_SUCCESS != ASIGetControlValue(0, ASI_TEMPERATURE, &temp2, &bAuto))
		fprintf(stderr, "could not retrieve sensor temperature\n");
	else {
		if (temp > -10000)
			temp = (temp + temp2) / 2;
		fprintf(stdout, "sensor temperature: %0.1f\n", temp/10.0f);
	}

	if (filename) {
		if (save_image(filename, buffer, w, h, params.exposure_ms, params.bin,
					params.offset, params.gain, &camera_info, start_time,
					temp > -10000 ? &temp : NULL)) {
			free(buffer);
			fprintf(stderr, "saving the image to '%s' failed\n", filename);
			ASICloseCamera(0);
			exit(1);
		}
	}

	if (image) {
		data_to_image_struct(image, (WORD *)buffer, 1, w, h, temp, start_time, &camera_info);
	}
}

void start_video_capture(exposure_params *params, int *rx, int *ry, ASI_CAMERA_INFO *camera_info) {
	if (params) {
		apply_exposure_params(*params, camera_info, rx, ry);
	}

	ASISetControlValue(0, ASI_BANDWIDTHOVERLOAD, 80, ASI_FALSE);
	ASISetControlValue(0, ASI_HIGH_SPEED_MODE, 1, ASI_FALSE);
	long temp;
	ASI_BOOL bAuto = ASI_FALSE;
	if (ASI_SUCCESS != ASIGetControlValue(0, ASI_HIGH_SPEED_MODE, &temp, &bAuto)) {
		fprintf(stderr, "could not retrieve speed mode\n");
	}
	fprintf(stdout, "HIGH SPEED MODE: %ld\n", temp);


	if (ASI_SUCCESS != ASIStartVideoCapture(0)) {
		fprintf(stderr, "failed to start video capture\n");
		ASICloseCamera(0);
		exit(1);
	}
}

void trigger_video() {
	if (ASI_SUCCESS != ASISendSoftTrigger(0, ASI_TRUE))
		fprintf(stderr, "trigger sending failed\n");
	else fprintf(stdout, "trigger sent\n");
}

void stop_video_capture(exposure_params params) {
	ASIStopVideoCapture(0);
}

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

/* image->camera_info must be already set, buffer is used as a preallocated buffer
 * start date is not documented
 */
int get_video_frame(image *image, int exposure_ms) {
	ASI_ERROR_CODE status;
	int timeout = max(exposure_ms * 2 + 500, 1000);
	if (ASI_SUCCESS != (status = ASIGetVideoData(0, (unsigned char*)image->data, image->rx * image->ry * 2, timeout))) {
		fprintf(stderr, "error '%s' while capturing frame *************\n", ASIError_tostr(status));
		return 1;
	}
	data_to_image_struct(image, image->data, 0, image->rx, image->ry, -10000, NULL, &image->camera_info);
	return 0;
}

/* convert a raw buffer to an image object, keeping only a monochrome version of the image */
void data_to_image_struct(image *image, WORD *buffer, int can_free_buffer, int w, int h, int temp, char *start_time, ASI_CAMERA_INFO *camera_info) {
	/* extract the green channel (average of the two greens in each Bayer matrix)
	 * For ASI cameras that have lower than 16 bits ADC, we won't lose
	 * information by storing the average of two values back in 16 bits.
	 * For others, too bad...
	 */
	// TODO: assuming image is top-down
	if (camera_info->IsColorCam == ASI_TRUE) {
		fprintf(stdout, "extracting green channel\n");
		int nx = w/2, ny = h/2;
		WORD *green = malloc(nx * ny * sizeof(WORD));
		if (!green) {
			fprintf(stderr, "could not allocate memory for demosaicing\n");
			exit(1);
		}

		int g_first = (camera_info->BayerPattern == ASI_BAYER_GR ||
				camera_info->BayerPattern == ASI_BAYER_GB);
		for (int y = 0; y < ny; y++) {
			for (int x = 0; x < nx; x++) {
				int sum = buffer[y * 2 * w + x * 2 + g_first ? 0 : 1];
				sum += buffer[(y * 2 + 1) * w + x * 2 + g_first];
				green[y * nx + x] = (WORD)(sum/2);
			}
		}

		if (can_free_buffer)
			free(buffer);
		buffer = green;
		w = nx;
		h = ny;
	}

	image->data = buffer;
	image->rx = w;
	image->ry = h;
	image->is_cfa = 0;
	image->start_time = start_time;
	image->temperature = temp;
	if (&image->camera_info != camera_info)
		memcpy(&image->camera_info, camera_info, sizeof(ASI_CAMERA_INFO));
}

/* convenience function */
int save_image2(const char *filename, image *image, exposure_params params) {
	return save_image(filename, (unsigned char *)image->data, image->rx, image->ry,
			params.exposure_ms, params.bin, params.offset, params.gain, &image->camera_info,
			image->start_time, image->temperature > -10000 ? &image->temperature : NULL);
}

void close_camera() {
	ASIPulseGuideOff(0, ASI_GUIDE_EAST);
	ASIPulseGuideOff(0, ASI_GUIDE_WEST);
	ASIPulseGuideOff(0, ASI_GUIDE_NORTH);
	ASIPulseGuideOff(0, ASI_GUIDE_SOUTH);
	ASICloseCamera(0);
}

const char *ASIError_tostr(ASI_ERROR_CODE err) {
	switch (err) {
		case ASI_SUCCESS:
			return "success";
		case ASI_ERROR_INVALID_INDEX:
			return "invalid index";
		case ASI_ERROR_INVALID_ID:
			return "invalid ID";
		case ASI_ERROR_INVALID_CONTROL_TYPE:
			return "invalid control type";
		case ASI_ERROR_CAMERA_CLOSED:
			return "camera is closed";
		case ASI_ERROR_CAMERA_REMOVED:
			return "camera removed";
		case ASI_ERROR_INVALID_PATH:
			return "invalid path";
		case ASI_ERROR_INVALID_FILEFORMAT:
			return "invalid file format";
		case ASI_ERROR_INVALID_SIZE:
			return "invalid size";
		case ASI_ERROR_INVALID_IMGTYPE:
			return "invalid image type";
		case ASI_ERROR_OUTOF_BOUNDARY:
			return "out of boundaries";
		case ASI_ERROR_TIMEOUT:
			return "timeout";
		case ASI_ERROR_INVALID_SEQUENCE:
			return "invalid sequence";
		case ASI_ERROR_BUFFER_TOO_SMALL:
			return "buffer is too small";
		case ASI_ERROR_VIDEO_MODE_ACTIVE:
			return "video mode is active";
		case ASI_ERROR_EXPOSURE_IN_PROGRESS:
			return "exposure is in progress";
		case ASI_ERROR_GENERAL_ERROR:
			return "general error";
		case ASI_ERROR_INVALID_MODE:
			return "invalid mode";
		case ASI_ERROR_END:
			return "END";
	}
	return "UNKNOWN ERROR";
}
