#include <gtk/gtk.h>
#include <glib.h>
#include <sys/time.h>
#include "tablet.h"
#include "image_display.h"
#include "mode_focus.h"
#include "peaker.h"
#include "PSF.h"

// small, medium and large exposures
int exposures[] = { 30, 300, 1500 };
gboolean zoomed_in = FALSE;

/* camera settings */
static exposure_params params;

struct stars_data {
	double fwhm;
	double roundness;
	int stars_count;
};

#define UNDEFINED_FHWM 99999.0
double last_values[50];
int nb_last_values = 0;
double min_known_fwhm = UNDEFINED_FHWM;

/* star detection parameters (same as siril) */
int radius = 40;	// default is 10, increase for high resolution
double ksigma = 3.0;	// only the brightest
double roundness_threshold = 0.20; // changing the focus will make the mirror or telescope move

/* star filtering settings */
double magnitude_threshold_from_brightest = 5.0; // keep only stars in the 5 first relative magnitudes
double minimum_plausible_fwhm = 6.0;	// below this value, it is probably a bright pixel or noise
int min_number_of_stars = 3;	// find the smallest area that contains as many bright stars

static volatile gboolean running = TRUE;
/* we have a thread reading images, a thread looking for stars in images, the
 * image buffer is passed from the first to the second and double buffering is
 * used */
static GThread *capture_thread = NULL, *working_thread = NULL;
static image *current_image = NULL;
static GMutex data_mutex;
static GCond data_cond;
#define EXIT_IMAGE_TOKEN ((void *)0x66)

static gpointer capture_worker(gpointer arg);
static gpointer star_finder_worker(gpointer arg);
struct stars_data *compute_fwhm_from_image(image *image);
void display_fwhm(struct stars_data *fwhm);

static void update_settings_label() {
	GtkLabel *label = GTK_LABEL(gtk_builder_get_object(builder, "explbl"));
	gchar *str = g_strdup_printf("%d ms exposure", params.exposure_ms);
	gtk_label_set_text(label, str);
	g_free(str);
}

static void restart_capture_to_update_params() {
	running = FALSE;
	stop_video_capture();
	g_thread_join(capture_thread);
	g_thread_join(working_thread);
	running = TRUE;
	current_image = NULL;
	capture_thread = g_thread_new("capture", capture_worker, NULL);
	working_thread = g_thread_new("findstar", star_finder_worker, NULL);
}

static void on_exposure_clicked(GtkButton *button, gpointer user_data) {
	int size = (char)GPOINTER_TO_INT(user_data);
	params.exposure_ms = exposures[size];
	fprintf(stdout, "set exposure to %d ms\n", params.exposure_ms);
	update_settings_label();
	restart_capture_to_update_params();
}

void mode_focus_stop() {
	running = FALSE;
	if (capture_thread)
		g_thread_join(capture_thread);
	if (working_thread)
		g_thread_join(working_thread);
}

void mode_focus_init() {
	g_signal_connect(gtk_builder_get_object(builder, "small_exposure"), "clicked", G_CALLBACK(on_exposure_clicked), GINT_TO_POINTER(0));
	g_signal_connect(gtk_builder_get_object(builder, "medium_exposure"), "clicked", G_CALLBACK(on_exposure_clicked), GINT_TO_POINTER(1));
	g_signal_connect(gtk_builder_get_object(builder, "large_exposure"), "clicked", G_CALLBACK(on_exposure_clicked), GINT_TO_POINTER(2));

	/* hiding the child of an overlay can only be done after the overlay has been displayed */
	GtkWidget *overlay = GTK_WIDGET(gtk_builder_get_object(builder, "focus_overlay_box"));
	gtk_widget_set_visible(overlay, FALSE);
}

void mode_focus_start(exposure_params _params) {
	params = _params;
	update_settings_label();
	current_image = NULL;
	capture_thread = g_thread_new("capture", capture_worker, NULL);
	working_thread = g_thread_new("findstar", star_finder_worker, NULL);

	GtkWidget *overlay = GTK_WIDGET(gtk_builder_get_object(builder, "focus_overlay_box"));
	gtk_widget_set_visible(overlay, TRUE);
}

static double elapsed_seconds(struct timeval start, struct timeval end) {
	double s = (double) (start.tv_sec + start.tv_usec / 1.0E6);
	double e = (double) (end.tv_sec + end.tv_usec / 1.0E6);
	return e - s;
}

static gpointer capture_worker(gpointer arg) {
	/* open a video stream */
	int nx, ny;
	ASI_CAMERA_INFO camera_info;
	start_video_capture(&params, &nx, &ny, &camera_info);
	long imsize = nx * ny * 2;

	// double-buffered image reading
	WORD *buffer1 = malloc(imsize);
	WORD *buffer2 = malloc(imsize);
	if (!buffer1 || !buffer2) {
		fprintf(stderr, "could not allocate streaming image buffer\n");
		exit(1);
	}
	image *image1 = calloc(1, sizeof(image)), *image2 = calloc(1, sizeof(image));
	if (!image1 || !image2) {
		fprintf(stderr, "could not allocate image\n");
		exit(1);
	}
	image1->rx = nx;
	image1->ry = ny;
	image1->data = buffer1;
	image1->camera_info = camera_info;
	memcpy(image2, image1, sizeof(image));
	image2->data = buffer2;

	image *current_im = image1;

	compute_pixel_mapping_to8();

	struct timeval tv1, tv2;
	gettimeofday(&tv1, NULL);

	while (running) {
		/* get a frame and compute FWHM */
		if (get_video_frame(current_im, params.exposure_ms))
			break;
		if (!running) break;
		gettimeofday(&tv2, NULL);
		double secs = elapsed_seconds(tv1, tv2);
		fprintf(stdout, "new frame after %.3f\n", secs);
		tv1 = tv2;

		/* convert to 8 bit pixbuf and queue redraw */
		map_image_to_surface(current_im, params.offset * 64);

		// send buffer to working thread
		g_mutex_lock (&data_mutex);
		current_image = current_im;
		g_cond_signal (&data_cond);
		g_mutex_unlock (&data_mutex);

		if (current_im == image1) {
			current_im = image2;
			/* manage green extracted buffer */
			if (image2->data != buffer2) {
				free(image2->data);
				image2->data = buffer2;
			}
		} else {
			current_im = image1;
			if (image1->data != buffer1) {
				free(image1->data);
				image1->data = buffer1;
			}
		}
		current_im->rx = nx;
		current_im->ry = ny;
	}

	// unblock the processing thread
	g_mutex_lock (&data_mutex);
	current_image = EXIT_IMAGE_TOKEN;
	g_cond_signal (&data_cond);
	g_mutex_unlock (&data_mutex);

	stop_video_capture();
	free(buffer1);
	free(buffer2);
	return 0;
}

// reuses the buffer
static void image_deep_copy(image *i1, image *i2) {
	memcpy(i1, i2, sizeof(image));
	long imsize = i2->rx * i2->ry * 2;
	// assuming image size never changes
	if (!i1->data) {
		i1->data = malloc(imsize);
		if (!i1->data) {
			fprintf(stderr, "could not allocate image memory\n");
			return;
		}
	}
	memcpy(i1->data, i2->data, imsize);
}

static gpointer star_finder_worker(gpointer arg) {
	image im = { 0 };
	int count = 0;

	while (running) {
		fprintf(stdout, "star finder waiting for an image\n");
		/* copy latest image to work on it */
		g_mutex_lock (&data_mutex);
		while (!current_image)
			g_cond_wait (&data_cond, &data_mutex);
		if (current_image == EXIT_IMAGE_TOKEN) {
			current_image = NULL;
			g_mutex_unlock (&data_mutex);
			break;
		}
		image_deep_copy(&im, current_image);
		current_image = NULL;
		g_mutex_unlock (&data_mutex);
		fprintf(stdout, "star finder got an image\n");

		/*if (++count % 5 == 0) {
			usleep(100000);
			trigger_video();
		}*/
		/* detect stars (not currently working) */
		//struct stars_data *fwhm = compute_fwhm_from_image(&im);
		//display_fwhm(fwhm);
	}
	fprintf(stdout, "stopping star detection\n");
	free(im.data);
	return NULL;
}

gboolean on_focus_draw_button_press_event(GtkWidget *widget,
		GdkEventButton *event, gpointer user_data) {
	if (event->button == GDK_BUTTON_PRIMARY) {
		fprintf(stdout, "click at %g, %g\n", event->x, event->y);
		/*GtkWidget *overlay = GTK_WIDGET(gtk_builder_get_object(builder, "focus_overlay_box"));
		gtk_widget_set_visible(overlay, !gtk_widget_get_visible(overlay)); */
		zoomed_in = !zoomed_in;
		if (zoomed_in) {
			double x, y;
			screen_to_fitted_image_coords(event->x, event->y, &x, &y);
			compute_transform(x, y, 2.0);
		}
		else compute_transform_to_fit();
		return TRUE;
	}
	return FALSE;
}

void on_min_fwhm_reset_button_clicked(GtkButton *button, gpointer user_data) {
	min_known_fwhm = UNDEFINED_FHWM;
}

struct stars_data *compute_fwhm_from_image(image *image) {
	fitted_PSF **stars = peaker(image->data, image->rx, image->ry,
			radius, ksigma, roundness_threshold);
	filter_stars(stars, minimum_plausible_fwhm, magnitude_threshold_from_brightest);
	struct stars_data *retval = malloc(sizeof(struct stars_data));
	retval->fwhm = UNDEFINED_FHWM;
	if (stars) {
		/* display results */
		int i;
		double fwhm_sum = 0.0, roundness_sum = 0.0;
		for (i = 0; stars[i]; i++) {
			fwhm_sum += stars[i]->fwhmx;
			roundness_sum += stars[i]->fwhmy / stars[i]->fwhmx;
		}
		if (i == 0)
			return retval;
		double fwhm = fwhm_sum/i;
		retval->fwhm = fwhm;
		retval->roundness = roundness_sum/i;
		retval->stars_count = i;
		fprintf(stdout, "\033[A\33[2KT\rFWHM: %3g (min %3g)\troundness: %2g (%d stars)%s\n",
				fwhm, min_known_fwhm, retval->roundness, i,
				fwhm < min_known_fwhm ? " NEW MINIMUM!" : "");
		free_fitted_stars(stars);
	}
	return retval;
}

gboolean display_fwhm_idle(gpointer p) {
	int values_length = sizeof last_values / sizeof(double);
	static GtkLabel *fwhmlbl = NULL, *minlbl = NULL;
	if (!fwhmlbl) {
		fwhmlbl = GTK_LABEL(gtk_builder_get_object(builder, "fwhmlbl"));
		minlbl = GTK_LABEL(gtk_builder_get_object(builder, "minfwhmlbl"));
	}
	double fwhm = ((struct stars_data *)p)->fwhm;

	if (fwhm != UNDEFINED_FHWM) {
		if (nb_last_values < values_length)
			last_values[nb_last_values++] = fwhm;
		else {
			// shift one
			for (int i = 0; i < values_length - 1; i++)
				last_values[i] = last_values[i+1];
			last_values[nb_last_values - 1] = fwhm;
		}

		if (min_known_fwhm > fwhm)
			min_known_fwhm = fwhm;
	}

	// display the values
	// TODO: display more than FWHM
	if (fwhm == UNDEFINED_FHWM)
		gtk_label_set_text(fwhmlbl, "---");
	else {
		char str[16];
		snprintf(str, 16, "%g", fwhm);
		gtk_label_set_text(fwhmlbl, str);
	}

	if (min_known_fwhm == UNDEFINED_FHWM)
		gtk_label_set_text(minlbl, "---");
	else {
		char str[16];
		snprintf(str, 16, "%g", min_known_fwhm);
		gtk_label_set_text(minlbl, str);
	}

	// TODO: display the history as a graph
	return FALSE;
}

void display_fwhm(struct stars_data *fwhm) {
	gdk_threads_add_idle(display_fwhm_idle, fwhm);
}
