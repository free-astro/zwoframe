#ifndef _GUIDE_H
#define _GUIDE_H

#include "wcs.h"

#define EARTH_DEGREES_PER_SECOND (1.0 / 239.344699)

/* parameters to add:
 * camera identifier: name? not sure ID is always the same
 */
typedef struct {
	const char *target_ra;	// double colon form, approximate
	const char *target_dec;

	double dec_guiding_speed;

	double max_pos_ra_speed;	// degrees per second
	double max_neg_ra_speed;
	double max_dec_speed;

	double backlash_time_ra;	// seconds

	double speed_change_distance;	// degrees
	double arcmin_consider_reached;
} guide_settings;

void guide_load_config(guide_settings **settings);

void execute_guiding_pulse(double ra_pulse, double dec_pulse, guide_settings *settings);
void compute_pulse_times(struct wcs *distance, double *ra_pulse, double *dec_pulse, guide_settings *settings);

#endif

