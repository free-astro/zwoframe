#!/bin/bash

basename=bias-o8-g120
if [[ $# == 1 ]]; then
	basename=$1
fi

index=1
while [[ 1 ]]; do
        filename=`echo "${basename}_${index}.fit"`
	if [ ! -f $filename ] ; then
		echo "capturing $filename"
		./zwoframe -b 1 -e 1 -g 120 -O 8 -o $filename || exit 1
	else
		echo "file $filename already exists, skipping"
	fi
	index=$(($index+1))
done
