#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <libconfig.h>
#include "ASICamera2.h"
#include "zwoasi.h"
#include "plate_solve.h"
#include "wcs.h"
#include "guide.h"

static exposure_params params = {
	.gain = 120,
	.exposure_ms = 1000,
	.bin = 1,
	.offset = 8 // [0, 100]
};

char *filename = "image.fit";

#define EXIT_ON_ERROR(call) \
	if (ASI_SUCCESS != call) { \
		fprintf(stderr, "error when calling %s\n", #call); \
		exit(1); \
	}

static void parse_args(int argc, char **argv);

plate_solve_settings * ps_settings;
guide_settings * g_settings;

struct wcs *target;

void get_current_coordinates(struct wcs *start, double *speed_ra, double *speed_dec) {
	struct wcs *current;
	image image;
	capture_frame(params, NULL, &image);
	save_image2(filename, &image, params);

	if (solve_image(filename, start, &current, ps_settings)) {
		fprintf(stderr, "field not solved, set target in config\n");
		close_camera();
		exit(1);
	}
}

void capture_solve_and_speed(struct wcs *start, double pulse_duration, double *speed_ra, double *speed_dec) {
	struct wcs *current;
	image image;
	capture_frame(params, NULL, &image);
	save_image2(filename, &image, params);
	if (solve_image(filename, start, &current, ps_settings)) {
		fprintf(stderr, "field not solved, set target in config\n");
		close_camera();
		exit(1);
	}
	double ra_change = start->alpha - current->alpha;
	double dec_change = start->delta - current->delta;
	if (speed_ra) {
		*speed_ra = ra_change / pulse_duration;
		fprintf(stdout, "guiding speed RA: %4g x sidereal\n",
				*speed_ra / EARTH_DEGREES_PER_SECOND);
		fprintf(stdout, "DEC drift over %g seconds: %4g'\n", pulse_duration, dec_change*60.0);
	}
	if (speed_dec) {
		*speed_dec = dec_change / pulse_duration;
		fprintf(stdout, "guiding speed DEC: %4g x sidereal\n",
				*speed_dec / EARTH_DEGREES_PER_SECOND);
		fprintf(stdout, "RA drift over %g seconds: %4g'\n", pulse_duration, ra_change*60.0);
	}
}

// guiding port test: 4 directions for 3 seconds
void guiding_port_test(int pulse_length_us, int measure_speed) {
	open_camera(1);
	// first turn all off in case of unwanted activation
	EXIT_ON_ERROR(ASIPulseGuideOff(0, ASI_GUIDE_EAST));
	EXIT_ON_ERROR(ASIPulseGuideOff(0, ASI_GUIDE_WEST));
	EXIT_ON_ERROR(ASIPulseGuideOff(0, ASI_GUIDE_NORTH));
	EXIT_ON_ERROR(ASIPulseGuideOff(0, ASI_GUIDE_SOUTH));

	struct wcs *start;
	double e_speed, w_speed, n_speed, s_speed;
	if (measure_speed) {
		fprintf(stdout, "using target from config file to help plate-solving: RA: %s, DEC: %s\n",
				g_settings->target_ra, g_settings->target_dec);
		struct wcs *target = wcs_from_colon_string(g_settings->target_ra, g_settings->target_dec);
		capture_frame(params, filename, NULL);
		if (solve_image(filename, target, &start, ps_settings)) {
			fprintf(stderr, "field not solved, set target in config\n");
			close_camera();
			exit(1);
		}
	}

	fprintf(stdout, "east\n");
	EXIT_ON_ERROR(ASIPulseGuideOn(0, ASI_GUIDE_EAST));
	usleep(pulse_length_us);
	EXIT_ON_ERROR(ASIPulseGuideOff(0, ASI_GUIDE_EAST));
	usleep(300000);
	if (measure_speed)
		capture_solve_and_speed(start, pulse_length_us/1000000.0, &e_speed, NULL);

	fprintf(stdout, "west\n");
	EXIT_ON_ERROR(ASIPulseGuideOn(0, ASI_GUIDE_WEST));
	usleep(pulse_length_us);
	EXIT_ON_ERROR(ASIPulseGuideOff(0, ASI_GUIDE_WEST));
	usleep((int)(g_settings->backlash_time_ra * 1000000));
	if (measure_speed)
		capture_solve_and_speed(start, pulse_length_us/1000000.0, &w_speed, NULL);

	fprintf(stdout, "north\n");
	EXIT_ON_ERROR(ASIPulseGuideOn(0, ASI_GUIDE_NORTH));
	usleep(pulse_length_us);
	EXIT_ON_ERROR(ASIPulseGuideOff(0, ASI_GUIDE_NORTH));
	usleep(300000);
	if (measure_speed)
		capture_solve_and_speed(start, pulse_length_us/1000000.0, NULL, &n_speed);

	fprintf(stdout, "south\n");
	EXIT_ON_ERROR(ASIPulseGuideOn(0, ASI_GUIDE_SOUTH));
	usleep(pulse_length_us);
	EXIT_ON_ERROR(ASIPulseGuideOff(0, ASI_GUIDE_SOUTH));
	usleep(300000);
	if (measure_speed)
		capture_solve_and_speed(start, pulse_length_us/1000000.0, NULL, &s_speed);

	fprintf(stdout, "Use this for the config file:\n+RA: %g, -RA: %g, +DEC: %g, -DEC: %g\n", e_speed, w_speed, n_speed, s_speed);

	close_camera();
}

struct wcs *capture_solve_and_distance(struct wcs *start) {
	struct wcs *current;
	capture_frame(params, filename, NULL);
	if (solve_image(filename, start, &current, ps_settings)) {
		fprintf(stderr, "field not solved, set target in config\n");
		close_camera();
		exit(1);
	}
	struct wcs *distance = malloc(sizeof(struct wcs));
	distance->alpha = start->alpha - current->alpha;
	distance->delta = start->delta - current->delta;
	fprintf(stdout, "RA distance: %4g', DEC distance: %4g'\n", distance->alpha*60.0, distance->delta*60.0);
	return distance;
}

int close_enough(struct wcs *distance) {
	return fabs(distance->alpha * 60.0) <= g_settings->arcmin_consider_reached &&
		fabs(distance->delta * 60.0) <= g_settings->arcmin_consider_reached;
}

void guide_to_target() {
	fprintf(stdout, "using target from config file to help plate-solving: RA: %s, DEC: %s\n",
			g_settings->target_ra, g_settings->target_dec);
	struct wcs *target = wcs_from_colon_string(g_settings->target_ra, g_settings->target_dec);
	while (1) {
		struct wcs *distance = capture_solve_and_distance(target);
		if (close_enough(distance)) {
			fprintf(stdout, "target reached! (distance: %3g', %3g')\n", distance->alpha*60.0, distance->delta*60.0);
			break;
		}
		double ra_pulse, dec_pulse;
		compute_pulse_times(distance, &ra_pulse, &dec_pulse, g_settings);
		fprintf(stdout, "computed pulses: %3g and %3g\n", ra_pulse, dec_pulse);
		execute_guiding_pulse(ra_pulse, dec_pulse, g_settings);
	}
}

/* to test the solver, provide a FITS image below and coordinates that are in
 * the image in the config file.
 */
void test_solve_from_image() {
	// test target: WASP-2 at 20:30:54, 06:25:46
	struct wcs *current, *target = wcs_from_colon_string(g_settings->target_ra, g_settings->target_dec);
	if (!target) {
		fprintf(stderr, "unknown target\n");
		exit(1);
	}
	if (solve_image("wasp2-2.fits", target, &current, ps_settings)) {
		fprintf(stderr, "failed\n");
		exit(1);
	}
	char *current_ra, *current_dec;
	wcs_to_pretty_print(current, &current_ra, &current_dec);
	fprintf(stdout, "current is %s, %s\n", current_ra, current_dec);

	double ra_distance = target->alpha - current->alpha;
	if (ra_distance > 180.0)
		ra_distance -= 360.0;
	double dec_distance = target->delta - current->delta;
	struct wcs distance = { .alpha = ra_distance, .delta = dec_distance };

	double ra_pulse, dec_pulse;
	compute_pulse_times(&distance, &ra_pulse, &dec_pulse, g_settings);

	char *distance_ra_arcsec, *distance_dec_arcsec;
	wcs_to_pretty_print(&distance, &distance_ra_arcsec, &distance_dec_arcsec);

	fprintf(stdout, "right ascension distance is %g (%s): requires a %g second %s pulse\n",
			ra_distance, distance_ra_arcsec, ra_pulse,
			ra_distance >= 0.0 ? "positive" : "negative");
	fprintf(stdout, "declination distance is %g (%s): requires a %g second %s pulse\n",
			dec_distance, distance_dec_arcsec, dec_pulse,
			dec_distance >= 0.0 ? "positive" : "negative");
}

int main(int argc, char **argv) {
	parse_args(argc, argv);
	solver_load_config(&ps_settings);
	guide_load_config(&g_settings);

	fputs("0 - guiding port test: 4 directions for 3 seconds (default)\n", stdout);
	fputs("1 - solver test\n", stdout);
	fputs("2 - guiding speed test: 10 seconds move, 5 seconds pause, 5 seconds exposure, 4 directions\n", stdout);
	fputs("3 - guite to target (same as GUI)\n", stdout);
	fputs("> ", stdout);
	char input[10];
	if (!fgets(input, 10, stdin)) {
		fputc('\n', stdout);
		return 0;
	}
	int choice = atoi(input);
	if (choice == 0)
		guiding_port_test(3000000, 0);
	else if (choice == 1)
		test_solve_from_image();
	else if (choice == 2)
		guiding_port_test(10000000, 1);
	else if (choice == 3)
		guide_to_target();
	return 0;
}

static void show_usage(char *program_name) {
	fprintf(stderr, "Usage: %s [-b binning_value] [-e exposure_in_milliseconds] [-g gain] [-o output_filename]\n", program_name);
	exit(1);
}

static void parse_args(int argc, char **argv) {
	int c, tmp;
	opterr = 0;
	while ((c = getopt (argc, argv, "b:e:g:o:")) != -1)
		switch (c)
		{
			case 'b':
				tmp = atoi(optarg);
				if (tmp > 0 && tmp < 10)
					params.bin = tmp;
				break;
			case 'e':
				tmp = atoi(optarg);
				if (tmp > 0)
					params.exposure_ms = tmp;
				break;
			case 'g':
				tmp = atoi(optarg);
				if (tmp > 0)
					params.gain = tmp;
				break;
			case 'o':
				filename = optarg;
				break;
			default:
				show_usage(*argv);
		}
}
