# zwoframe

Headless ZWO ASI capture software for linux, single frame to FITS.

Supports setting exposure, gain, binning (for monochrome), offset and filename.

Usage: `Usage: %s [-b binning_value] [-e exposure_in_milliseconds] [-g gain] [-O offset] [-o output_filename]`

Example:
```shell
zwoframe -b 2 -e 30000 -g 120 -o light.fit
```

## Additional programs

Additional programs are being developed:
- `guidetotarget` will use the ST4 guiding port of the connected camera and
plate-solving with astrometry.net to slew the telescope to a target object.
Some of its settings are in the configuration file named `config`
- `focusaid` will display an updated FWHM value averaged near the center of the
image to help manual focus
- `tablet` is a touch display program that manages acquisition including the
two above, especially made for the raspberry pi 4 and its official display

## Dependencies
All programs depend on the ZWO ASI SDK. It should be installed in the system.
To simplify, they also all depend on cfitsio.

`guidetotarget` additionally depends on libconfig.

`focusaid` additionally depends on the GNU scientific library (gsl) .

`tablet` additionally depends on GTK+


Author: Vincent Hourdin (free-astro.org)
