/* this is an example program for gtk stack and overlay with touch input */
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

static GtkBuilder *builder;
static int current = 0;


static void set_current(int page) {
	if (current == page || page < 0 || page > 2)
		return;
	GtkStack *stack = GTK_STACK(gtk_builder_get_object(builder, "stack"));
	if (current > page)
		gtk_stack_set_transition_type(stack, GTK_STACK_TRANSITION_TYPE_SLIDE_RIGHT);
	else 	gtk_stack_set_transition_type(stack, GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT);

	gtk_stack_set_visible_child_name(stack, page == 0 ? "page0" : (page == 1 ? "page1" : "page2"));
	current = page;
}

/* for gestures, see this example, it's quite simple:
 * https://fossies.org/linux/gtk+/demos/gtk-demo/gestures.c
 */
static void swipe_gesture_swept (GtkGestureSwipe *gesture,
		gdouble          velocity_x,
		gdouble          velocity_y,
		GtkWidget       *widget) {
	fprintf(stdout, "swipe detected %g, %g\n", velocity_x, velocity_y);
	if (velocity_x < -500.0)
		set_current(current+1);
	else if (velocity_x > -500.0)
		set_current(current-1);
	GtkWidget *overlay = GTK_WIDGET(gtk_builder_get_object(builder, "box"));
	if (velocity_y > 500.0)
		gtk_widget_set_visible(overlay, TRUE);
	else if (velocity_y < -500.0)
		gtk_widget_set_visible(overlay, FALSE);
}

// called by GTK when the window shows up
static void on_activate (GtkApplication *app) {
	builder = gtk_builder_new();
	GError *err = NULL;
	if (!gtk_builder_add_from_file(builder, "stackoverlay.glade", &err)) {
		fprintf(stderr, "failed to load the glade file: %s\n", err->message);
		exit(1);
	}

	GObject* window = gtk_builder_get_object(builder, "main_window");
	if (!window) {
		fprintf(stderr, "window not found\n");
		return;
	}
	g_object_set (gtk_settings_get_default (), "gtk-enable-animations", TRUE, NULL);

	gtk_window_set_application(GTK_WINDOW(window), app);
	gtk_builder_connect_signals(builder, NULL);

	GtkWidget *stack = GTK_WIDGET(gtk_builder_get_object(builder, "stack"));
	GtkGesture *gesture = gtk_gesture_swipe_new(stack);
	g_signal_connect(gesture, "swipe",
			G_CALLBACK (swipe_gesture_swept), stack);
	gtk_event_controller_set_propagation_phase (GTK_EVENT_CONTROLLER(gesture),
			GTK_PHASE_BUBBLE);
	g_object_weak_ref (G_OBJECT(stack), (GWeakNotify) g_object_unref, gesture);

	gtk_widget_show_all(GTK_WIDGET(window));
	//gtk_window_fullscreen(GTK_WINDOW(window));
}

void on_button1_clicked(GtkButton *button, gpointer user_data) {
	fprintf(stdout, "button 1 clicked\n");
	set_current(0);
}

void on_button2_clicked(GtkButton *button, gpointer user_data) {
	fprintf(stdout, "button 2 clicked\n");
	set_current(1);
}

void on_button3_clicked(GtkButton *button, gpointer user_data) {
	fprintf(stdout, "button 3 clicked\n");
	set_current(2);
}

int main(int argc, char **argv) {
	GtkApplication *app = gtk_application_new("org.free-astro.zwoframe.gui",
			G_APPLICATION_FLAGS_NONE);
	g_signal_connect (app, "activate", G_CALLBACK(on_activate), NULL);

	int status = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);
	return status;
}

