/* this is an example program for gtk overlays */
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

static GtkBuilder *builder;

// called by GTK when the window shows up
static void on_activate (GtkApplication *app) {
	builder = gtk_builder_new();
	GError *err = NULL;
	if (!gtk_builder_add_from_file(builder, "overlay.glade", &err)) {
		fprintf(stderr, "failed to load the glade file: %s\n", err->message);
		exit(1);
	}
	GObject* window = gtk_builder_get_object(builder, "main_window");
	if (!window) {
		fprintf(stderr, "window not found\n");
		return;
	}
	gtk_window_set_application(GTK_WINDOW(window), app);
	gtk_builder_connect_signals(builder, NULL);
	gtk_widget_show_all(GTK_WIDGET(window));
	//gtk_window_fullscreen(GTK_WINDOW(window));

	// TODO: to collapse the overlay, and expander can be used, but the title remains
}

gboolean redraw_drawingarea(GtkWidget *widget, cairo_t *cr, gpointer data) {
	fprintf(stdout, "redraw\n");
	cairo_set_source_rgb(cr, 0.04, 0.04, 0.04);
	cairo_paint(cr);
	cairo_set_source_rgba(cr, 1.0, 0.4, 0.0, 1.0);
	cairo_set_line_width(cr, 2.0);
	cairo_arc(cr, 100.0, 100.0, 100.0, 0., 2. * M_PI);
	cairo_stroke(cr);
	double hwidth = gtk_widget_get_allocated_width(widget) * 0.5;
        double hheight = gtk_widget_get_allocated_height(widget) * 0.5;
	cairo_arc(cr, hwidth, hheight, fmin(hwidth, hheight), 0., 2. * M_PI);
	cairo_stroke(cr);
	cairo_arc(cr, hwidth, hheight, 200.0, 0., 2. * M_PI);
	cairo_stroke(cr);
	return FALSE;
}

gboolean on_main_draw_button_press_event(GtkWidget *widget,
		GdkEventButton *event, gpointer user_data) {
	if (event->button == GDK_BUTTON_PRIMARY) {
		fprintf(stdout, "click\n");
		GtkWidget *overlay = GTK_WIDGET(gtk_builder_get_object(builder, "box"));
		gtk_widget_set_visible(overlay, !gtk_widget_get_visible(overlay));
		return FALSE;
	}
	return FALSE;
}

void on_exposure_clicked(GtkButton *button, gpointer user_data) {
	fprintf(stdout, "button clicked\n");
}

int main(int argc, char **argv) {
	GtkApplication *app = gtk_application_new("org.free-astro.zwoframe.gui",
			G_APPLICATION_FLAGS_NONE);
	g_signal_connect (app, "activate", G_CALLBACK(on_activate), NULL);

	int status = g_application_run(G_APPLICATION(app), argc, argv);
	g_object_unref(app);
	return status;
}

