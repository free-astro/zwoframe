#ifndef _ZWO_ASI_HELPER_
#define _ZWO_ASI_HELPER_

#include "ASICamera2.h"

typedef unsigned short WORD;

typedef struct {
	int gain;
	int exposure_ms;
	int bin;
	int offset;	// [0, 100]
} exposure_params;

typedef struct {
	WORD *data;
	int rx;
	int ry;
	int is_cfa;
	//ASI_BAYER_PATTERN pattern;

	// useful for saving
	char *start_time;	// exposure start, use get_time()
	long temperature;	// <= -10000 is undefined
	ASI_CAMERA_INFO camera_info;
} image;

typedef struct {
	int identifier;	// always 0 for now
	char *name;
	int is_osc;
	int has_guiding_port;
	int is_open;
} camera_capabilities;

const char *ASIError_tostr(ASI_ERROR_CODE err);

void open_camera(int check_guiding);	// required before any other camera call
void close_camera();			// to call at the end of programs
camera_capabilities *get_camera_info();

void capture_frame(exposure_params params, const char *filename, image *image);

/* if not provided, the video capture params will be the same as a previous
 * capture_frame call */
void start_video_capture(exposure_params *params, int *rx, int *ry, ASI_CAMERA_INFO *camera_info);
int get_video_frame(image *image, int exposure_ms);
void stop_video_capture();
void trigger_video();

int save_image2(const char *filename, image *image, exposure_params params);

void data_to_image_struct(image *image, WORD *buffer, int can_free_buffer, int w, int h, int temp, char *start_time, ASI_CAMERA_INFO *camera_info);

#endif
