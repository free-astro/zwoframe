#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <libconfig.h>
#include "wcs.h"
#include "plate_solve.h"

#define CONF_FILENAME "solver_config"
#define DEBUG_SOLVER 1	// set to 0 to disable

void solver_load_config(plate_solve_settings **settings) {
	config_t config;
        config_init(&config);
        if (config_read_file(&config, CONF_FILENAME) == CONFIG_FALSE) {
		fprintf(stderr, "could not load config file `%s'\n", CONF_FILENAME);
		config_destroy(&config);
		*settings = NULL;
		return;
        }

	*settings = malloc(sizeof(plate_solve_settings));
	config_lookup_string(&config, "arcsec_per_pixel_low", &(*settings)->arcsec_per_pixel_low);
	config_lookup_string(&config, "arcsec_per_pixel_high", &(*settings)->arcsec_per_pixel_high);
	config_lookup_string(&config, "image_parity", &(*settings)->parity);
	(*settings)->debug = DEBUG_SOLVER;
}

/* call astrometry.net solve-field on the current image to find current coordinates */
int solve_image(const char *current_image_name, struct wcs *target, struct wcs **coordinates, plate_solve_settings *settings) {
	/* build the command line */
	char *target_ra, *target_dec;
	wcs_to_triple_colon_string(target, &target_ra, &target_dec);
	char *args[] = { "solve-field", "-p", "-O", "--fits-image", "-u", "arcsecperpix", "-L", (char *)settings->arcsec_per_pixel_low, "-H", (char *)settings->arcsec_per_pixel_high, "--ra", target_ra, "--dec", target_dec, "--radius", "30", "-z", "2", "-y", "-T", "-d", "50", "-D", "/tmp", "-o", "astrometry", "-N", "none", "--axy", "none", "-S", "none", "-M", "none", "-R", "none", "-B", "none", "-j", "none", "-W", "none", "--parity", (char *)settings->parity, (char *)current_image_name, NULL };

	char *solved_coords = NULL;
	fputs("solving field...\n", stdout);

	/* call solve-field */
	int pipefd[2];
	if (pipe(pipefd)) {
		perror("failed to create pipe for solve-field");
		return -1;
	}
	int pid = fork();
	if (pid == -1) {
		perror("failed to fork for solve-field");
		return -1;
	}
	if (pid == 0) {
		close(pipefd[0]);
		dup2(pipefd[1], 1);
		dup2(pipefd[1], 2);
		close(pipefd[1]);
		if (execvp(args[0], args)) {
			perror("could not execute solve-field");
			return -1;
		}
	}
	else {
		char buffer[1024];
		close(pipefd[1]);
		FILE *file = fdopen(pipefd[0], "r");
		if (!file) {
			perror("fdopen");
			waitpid(pid, NULL, 0);
			close(pipefd[0]);
			return -1;
		}
		while (fgets(buffer, 1024, file)) {
			if (settings->debug)
				fprintf(stdout, "solver: %s", buffer);
			if (!strncmp(buffer, "Did not solve", strlen("Did not solve"))) {
				if (settings->debug)
					fputs("Did not solve\n", stdout);
				break;
			}
			if (!strncmp(buffer, "Field center: (RA,Dec)", 22)) {
				solved_coords = strdup(buffer);
			}
		}
		waitpid(pid, NULL, 0);
		if (settings->debug)
			fputs("solver exited\n", stdout);
		fclose(file);
		close(pipefd[0]);
	}

	/* parse output */
	if (solved_coords) {
		if (settings->debug)
			fprintf(stdout, "solve-field gave: %s", solved_coords);
		// "Field center: (RA,Dec) = (307.687909, 6.235676) deg."
		char *ra_start = strchr(solved_coords, '(');
		if (!ra_start) return 1;
		ra_start = strchr(ra_start + 1, '(');
		if (!ra_start) return 1;
		ra_start++;
		char *ra_end = strchr(ra_start, ',');
		if (!ra_end) return 1;
		ra_end[0] = '\0';

		double ra = strtod(ra_start, &ra_end);
		if (ra_end == ra_start)
			return 1;

		char *dec_start = ra_end + 1;
		while (*dec_start == ' ' || *dec_start == '\t')
			dec_start++;
		char *dec_end = strchr(dec_start, ')');
		dec_end[0] = '\0';

		double dec = strtod(dec_start, &dec_end);
		if (dec_end == dec_start)
			return 1;

		*coordinates = malloc(sizeof(struct wcs));
		(*coordinates)->alpha = ra;
		(*coordinates)->delta = dec;
		return 0;
	}
	return 1;
}


