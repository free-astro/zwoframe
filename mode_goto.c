#include <gtk/gtk.h>
#include <glib.h>
#include "tablet.h"
#include "image_display.h"
#include "mode_goto.h"
#include "wcs.h"
#include "plate_solve.h"
#include "guide.h"

/* camera settings */
static exposure_params params;

char *filename = "image.fit";

int current_speed = -1;

#define CONF_FILENAME "config"

#define EARTH_DEGREES_PER_SECOND (1.0 / 239.344699)

guide_settings * g_settings;
plate_solve_settings * ps_settings;

struct wcs *target;


void mode_goto_stop() {
	//running = FALSE;
}

void mode_goto_init() {
	solver_load_config(&ps_settings);
	guide_load_config(&g_settings);
}


void mode_goto_start(exposure_params _params) {
	params = _params;

	GtkWidget *overlay = GTK_WIDGET(gtk_builder_get_object(builder, "focus_overlay_box"));
	gtk_widget_set_visible(overlay, TRUE);
}

