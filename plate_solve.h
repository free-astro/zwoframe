#ifndef _PLATE_SOLVE_H
#define _PLATE_SOLVE_H

#include "wcs.h"

typedef struct {
	const char *arcsec_per_pixel_low; // as string
	const char *arcsec_per_pixel_high;
	const char *parity;	// "pos" or "neg"
	int debug;		// boolean for troubleshooting
} plate_solve_settings;

void solver_load_config(plate_solve_settings **settings);

int solve_image(const char *current_image_name, struct wcs *target, struct wcs **coordinates, plate_solve_settings *settings);

#endif

