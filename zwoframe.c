#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include "zwoasi.h"

static exposure_params params = {
	.gain = 120,
	.exposure_ms = 1000,
	.bin = 1,
	.offset = 8 // [0, 100]
};

int debayer_green = 0;
char *filename = "image.fit";

static void parse_args(int argc, char **argv);

int main(int argc, char **argv) {
	parse_args(argc, argv);

	open_camera(0);
	capture_frame(params, filename, NULL);
	close_camera();
	return 0;
}

static void show_usage(char *program_name) {
	fprintf(stderr, "Usage: %s [-b binning_value] [-e exposure_in_milliseconds] [-g gain] [-O offset] [-o output_filename]\n", program_name);
	exit(1);
}

static void parse_args(int argc, char **argv) {
	int c, tmp;
	opterr = 0;
	static struct option long_options[] = {
                   { "green", no_argument, 0, 0 }
	};
	while ((c = getopt_long (argc, argv, "O:b:e:g:o:", long_options, NULL)) != -1)
		switch (c)
		{
			case 0:
				debayer_green = 1;
				break;
			case 'O':
				tmp = atoi(optarg);
				if (tmp >= 0 && tmp <= 100)
					params.offset = tmp;
				break;
			case 'b':
				tmp = atoi(optarg);
				if (tmp > 0 && tmp < 10)
					params.bin = tmp;
				break;
			case 'e':
				tmp = atoi(optarg);
				if (tmp > 0)
					params.exposure_ms = tmp;
				break;
			case 'g':
				tmp = atoi(optarg);
				if (tmp > 0)
					params.gain = tmp;
				break;
			case 'o':
				filename = optarg;
				break;
			default:
				show_usage(*argv);
		}
}
