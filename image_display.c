#include <gtk/gtk.h>
#include <glib.h>
#include <math.h>
#include "image_display.h"
#include "tablet.h"
#include "zwoasi.h"

GtkWidget *drawingarea = NULL;
guchar *cairo_buffer = NULL;
int surface_stride;
cairo_surface_t *surface = NULL;

int image_width = 0, image_height = 0;
cairo_matrix_t display_transform;
gboolean transform_set = FALSE;

guchar to8_mapping[65536];

static void declare_cairo_surface(int w, int h);

guchar roundf_to_BYTE(float f) {
	if (f < 0.5f) return 0;
	if (f >= 254.5) return 255;
	return (guchar)(f + 0.5f);
}

void compute_pixel_mapping_to8() {
	//float slope = 255.0f / sqrtf(65535.0f);
	int i;
	for (i = 0; i <= USHRT_MAX; i++) {
		to8_mapping[i] = roundf_to_BYTE(sqrtf((float)i));
		if (to8_mapping[i] == UCHAR_MAX) {
			fprintf(stdout, "8 bit mapping saturating at %d\n", i);
			break;
		}
	}
	if (i != USHRT_MAX + 1) {
		/* no more computation needed, just fill with max value */
		for (++i; i <= USHRT_MAX; i++)
			to8_mapping[i] = UCHAR_MAX;
	}
}

gboolean redraw_idle(gpointer p) {
	if (!drawingarea)
		drawingarea = GTK_WIDGET(gtk_builder_get_object(builder, "focus_draw"));
	gtk_widget_queue_draw(drawingarea);
	return FALSE;
}

void map_image_to_surface(image *image, int lo) {
	declare_cairo_surface(image->rx, image->ry);
	if (!cairo_buffer)
		return;
	int nbdata = image->rx * image->ry;
//#pragma omp parallel for schedule(static)
	for (int i = 0; i < nbdata; i++) {
		WORD input = image->data[i];
		input = input < lo ? 0 : input - lo;
		guchar pixval = to8_mapping[input];
		if (pixval == 255)
			((guint32*)cairo_buffer)[i] = 0x00ff0000;
		else ((guint32*)cairo_buffer)[i] = pixval << 16 | pixval << 8 | pixval;
	}
	cairo_surface_flush(surface);
	cairo_surface_mark_dirty(surface);
	gdk_threads_add_idle(redraw_idle, NULL);
}

static void declare_cairo_surface(int w, int h) {
	if (w == image_width && h == image_height)
		return;
	fprintf(stdout, "setting image surface to %d x %d\n", w, h);
	surface_stride = cairo_format_stride_for_width(CAIRO_FORMAT_RGB24, w);
	guchar *new_buffer = realloc(cairo_buffer, surface_stride * h);
	if (!new_buffer) {
		free(cairo_buffer);
		cairo_buffer = NULL;
		image_width = 0;
		fprintf(stderr, "image buffer allocation failed\n");
		return;
	}
	cairo_buffer = new_buffer;
	if (surface)
		cairo_surface_destroy(surface);
	surface = cairo_image_surface_create_for_data(cairo_buffer, CAIRO_FORMAT_RGB24,
		w, h, surface_stride);
	if (cairo_surface_status(surface) != CAIRO_STATUS_SUCCESS) {
		fprintf(stderr, "cairo surface creation failed\n");
		surface = NULL;
		free(cairo_buffer);
		cairo_buffer = NULL;
		image_width = 0;
		return;
	}
	cairo_matrix_init_identity(&display_transform);
	image_width = w;
	image_height = h;
}

static double get_fit_zoom_val(GtkWidget *widget) {
	int window_width = gtk_widget_get_allocated_width(widget);
        int window_height = gtk_widget_get_allocated_height(widget);
        if (image_width == 0 || image_height == 0 || window_height <= 1 || window_width <= 1)
                return 1.0;
        double wtmp = (double)window_width / (double)image_width;
        double htmp = (double)window_height / (double)image_height;
	return fmin(wtmp, htmp);
}

void screen_to_fitted_image_coords(double x, double y, double *tox, double *toy) {
	if (!drawingarea)
		drawingarea = GTK_WIDGET(gtk_builder_get_object(builder, "focus_draw"));
	double zoom = get_fit_zoom_val(drawingarea);
	*tox = x / zoom;
	*toy = y / zoom;
}

static void centre_coords_on_screen(GtkWidget *widget, double x, double y, double zoom, double *tox, double *toy) {
	int window_width = gtk_widget_get_allocated_width(widget);
        int window_height = gtk_widget_get_allocated_height(widget);
	*tox = x * zoom - window_width / 2;
	*toy = y * zoom - window_height/ 2;
}

void compute_transform_to_fit() {
	if (!drawingarea)
		drawingarea = GTK_WIDGET(gtk_builder_get_object(builder, "focus_draw"));
	double zoom = get_fit_zoom_val(drawingarea);
	fprintf(stdout, "displaying image to fit, zoom level %f\n", zoom);
        cairo_matrix_init(&display_transform, zoom, 0, 0, zoom, 0, 0);
	transform_set = TRUE;
}

// x and y are the centre coordinates, not the corner's
void compute_transform(double x, double y, double zoom) {
	if (!drawingarea)
		drawingarea = GTK_WIDGET(gtk_builder_get_object(builder, "focus_draw"));

	double corner_x, corner_y;
	centre_coords_on_screen(drawingarea, x, y, zoom, &corner_x, &corner_y);
        cairo_matrix_init(&display_transform,
                        zoom, 0, 0, zoom, -corner_x, -corner_y);
	transform_set = TRUE;
}

gboolean redraw_drawingarea(GtkWidget *widget, cairo_t *cr, gpointer data) {
	//fprintf(stdout, "redraw\n");
	if (!cairo_buffer) return FALSE;
	if (!transform_set) {
		compute_transform_to_fit();
	}

	cairo_transform(cr, &display_transform);
	cairo_set_source_surface(cr, surface, 0, 0);
	cairo_pattern_set_filter(cairo_get_source(cr), CAIRO_FILTER_FAST); // or GOOD
	cairo_paint(cr);
	return FALSE;
}
