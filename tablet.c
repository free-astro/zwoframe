/*
 * This file is the start of a development of a tablet-controlled ZWO camera
 * acquisition program. It is developed and tested with a Raspberry Pi and its
 * officiel touch display.
 *
 * It features some code from Siril, which is licensed as GPL v3, so is this
 * program.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ASICamera2.h>
#include <gtk/gtk.h>
#include <glib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "zwoasi.h"
#include "tablet.h"
#include "mode_focus.h"
#include "mode_goto.h"

/* general camera settings */
static exposure_params params = {
	.gain = 300,
	.exposure_ms = 700,
	.bin = 1,
	// binning is used as 1 in this program, but for color cameras,
	// demosaicing is done with averaging the two green pixels of the Bayer
	// square, so the result in image size is like a bin2
	.offset = 8 // [0, 100]
};

GtkBuilder *builder = NULL;

static void parse_args(int argc, char **argv);
static void update_brightness_gui();
static void set_cursor_waiting(gboolean wait);

static void camera_init_and_update_gui() {
	GtkLabel *camlbl = GTK_LABEL(gtk_builder_get_object(builder, "camera_info_label"));
	GtkWidget *modes_box = GTK_WIDGET(gtk_builder_get_object(builder, "mode_buttons_box"));
	set_cursor_waiting(TRUE);
	camera_capabilities *cap = get_camera_info();
	if (!cap) {
		gtk_label_set_text(camlbl, "No camera found");
		gtk_widget_set_sensitive(modes_box, FALSE);
		set_cursor_waiting(FALSE);
		return;
	}
	gchar *msg = g_strdup_printf("Camera found%s: %s (%s, %s guiding port)",
			cap->is_open ? "" : " but failed to be opened",
			cap->name,
			cap->is_osc ? "color" : "monochrome",
			cap->has_guiding_port ? "has" : "no");
	gtk_label_set_text(camlbl, msg);
	gtk_widget_set_sensitive(modes_box, cap->is_open);
	g_free(msg);
	free(cap);
	set_cursor_waiting(FALSE);
}

void on_refresh_camera_button_clicked(GtkButton *button, gpointer user_data) {
	camera_init_and_update_gui();
}

void on_mode_focus_button_clicked(GtkButton *button, gpointer user_data) {
	// hide the main overlay
	GtkWidget *overlay = GTK_WIDGET(gtk_builder_get_object(builder, "main_overlay_box"));
	gtk_widget_set_visible(overlay, FALSE);

	mode_focus_start(params);
}

void on_mode_goto_button_clicked(GtkButton *button, gpointer user_data) {
	// hide the main overlay
	GtkWidget *overlay = GTK_WIDGET(gtk_builder_get_object(builder, "main_overlay_box"));
	gtk_widget_set_visible(overlay, FALSE);

	mode_goto_start(params);
}

void destroy(GtkWidget* widget, gpointer data)
{
    g_application_quit(G_APPLICATION(data));
}

// called by GTK when the window shows up
static void on_activate (GtkApplication *app) {
	builder = gtk_builder_new();
	GError *err = NULL;
	if (!gtk_builder_add_from_file(builder, "tablet.glade", &err)) {
		fprintf(stderr, "failed to load the glade file: %s\n", err->message);
		exit(1);
	}
	GObject* window = gtk_builder_get_object(builder, "main_window");
	if (!window) {
		fprintf(stderr, "window not found\n");
		return;
	}
	gtk_window_set_application(GTK_WINDOW(window), app);
	gtk_builder_connect_signals(builder, NULL);

	/* general overlay */
	g_signal_connect(GTK_BUTTON(gtk_builder_get_object(builder, "quit_button")),
			"clicked", G_CALLBACK(destroy), app);
	g_signal_connect(window, "destroy", G_CALLBACK(destroy), app);
	// same with control-q
	GtkAccelGroup *accel_group = gtk_accel_group_new();
	gtk_window_add_accel_group(GTK_WINDOW(window), accel_group);
	gtk_accel_group_connect(accel_group, GDK_KEY_Q, GDK_CONTROL_MASK, 0, g_cclosure_new_swap(G_CALLBACK(g_application_quit), app, NULL));

	update_brightness_gui();
	camera_init_and_update_gui();

	gtk_widget_show_all(GTK_WIDGET(window));
	//gtk_window_fullscreen(GTK_WINDOW(window));

	mode_focus_init();
	mode_goto_init();
}

int main(int argc, char **argv) {
	parse_args(argc, argv);

	GtkApplication *app = gtk_application_new("org.free-astro.zwoframe.gui",
			G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(on_activate), NULL);

	int status = g_application_run(G_APPLICATION(app), argc, argv);

	fprintf(stdout, "application exiting...\n");
	mode_focus_stop();
	mode_goto_stop();
	close_camera();
	g_object_unref(app);
	return status;
}

static void show_usage(char *program_name) {
	fprintf(stderr, "Usage: %s [-e exposure_in_milliseconds] [-g gain]\n", program_name);
	exit(1);
}

static void parse_args(int argc, char **argv) {
	int c, tmp;
	opterr = 0;
	while ((c = getopt (argc, argv, "e:g:")) != -1)
		switch (c)
		{
			case 'e':
				tmp = atoi(optarg);
				if (tmp > 0)
					params.exposure_ms = tmp;
				break;
			case 'g':
				tmp = atoi(optarg);
				if (tmp > 0)
					params.gain = tmp;
				break;
			default:
				show_usage(*argv);
		}
}

/* Managing the Raspberry Pi official 7" Touch Display */
static void update_brightness_gui() {
	GtkWidget *scalewidget = GTK_WIDGET(gtk_builder_get_object(builder, "rpitd_brightness_scale"));
	GtkAdjustment *adj = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "adj_brightness"));
	gboolean can_write = FALSE;
	int fd = open("/sys/class/backlight/rpi_backlight/brightness", O_RDWR);
	if (fd == -1) {
		if ((fd = open("/sys/class/backlight/rpi_backlight/brightness", O_RDONLY)) == -1) {
			fprintf(stdout, "no rasberry pi display found\n");
			gtk_widget_set_sensitive(scalewidget, FALSE);
			return;
		}
		fprintf(stdout, "raspberry pi display brightness cannot be set by current user, add a udev rule\n");
		// something like this:
		// SUBSYSTEM=="backlight",RUN+="/bin/chmod 666 /sys/class/backlight/%k/brightness /sys/class/backlight/%k/bl_power
	} else can_write = TRUE;

	int len, current;
	char buf[20];
	if ((len = read(fd, buf, 19)) == -1) {
		gtk_widget_set_sensitive(scalewidget, FALSE);
		fprintf(stdout, "error reading rasberry pi display brightness\n");
		return;
	}

	buf[len] = '\0';
	current = atoi(buf);
	if (len == 0 || (current == 0 && len == 0)) {
		gtk_widget_set_sensitive(scalewidget, FALSE);
		fprintf(stdout, "error reading rasberry pi display brightness\n");
		return;
	}

	gtk_adjustment_set_value(adj, (double)current);
	gtk_widget_set_sensitive(scalewidget, can_write);
}


static void set_cursor_waiting(gboolean wait) {
	GdkDisplay *display = gdk_display_get_default();
	GdkScreen *screen = gdk_screen_get_default();
	GdkCursor *cursor = NULL;
	if (wait)
		cursor = gdk_cursor_new_from_name(display, "progress");

	GList *list = gdk_screen_get_toplevel_windows(screen);
	for (GList *l = list; l; l = l->next) {
		GdkWindow *window = GDK_WINDOW(l->data);
		gdk_window_set_cursor(window, cursor);
		gdk_display_sync(gdk_window_get_display(window));
	}

	gdk_display_flush(display);
	g_list_free(list);
}

