#ifndef _IMAGE_DISPLAY_H
#define _IMAGE_DISPLAY_H

#include "zwoasi.h"

void compute_pixel_mapping_to8();
void map_image_to_surface(image *image, int lo);

void screen_to_fitted_image_coords(double x, double y, double *tox, double *toy);
void compute_transform_to_fit();
void compute_transform(double x, double y, double zoom);

#endif
