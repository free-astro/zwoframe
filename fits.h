#ifndef _FITS_H_
#define _FITS_H_

extern char* ASI_bayer_patterns[];

int save_image(const char *filename, unsigned char *buffer, int w, int h,
		int exposure_ms, int bin, int offset, int gain,
		ASI_CAMERA_INFO *camera_info, char *start_time, long *temperature);
int read_fits(char *filename, unsigned char **buffer, int *w, int *h, int *is_rgb, ASI_BAYER_PATTERN *pattern);
char *get_time();

#endif
