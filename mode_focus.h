#ifndef _MODE_FOCUS_H
#define _MODE_FOCUS_H

#include "zwoasi.h"

void mode_focus_init();
void mode_focus_start(exposure_params _params);
void mode_focus_stop();

#endif
