#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>
#include <libconfig.h>
#include "wcs.h"
#include "guide.h"
#include "ASICamera2.h"

#define CONF_FILENAME "guiding_config"


static int current_speed = -1;

void guide_load_config(guide_settings **settings) {
	config_t config;
        config_init(&config);
        if (config_read_file(&config, CONF_FILENAME) == CONFIG_FALSE) {
		fprintf(stderr, "could not load config file `%s'\n", CONF_FILENAME);
		config_destroy(&config);
		return;
        }
	
	*settings = malloc(sizeof(guide_settings));

	config_lookup_string(&config, "target_ra", &(*settings)->target_ra);
	config_lookup_string(&config, "target_dec", &(*settings)->target_dec);
	fprintf(stdout, "configured target is %s, %s\n", (*settings)->target_ra, (*settings)->target_dec);

	config_lookup_float(&config, "dec_guiding_speed", &(*settings)->dec_guiding_speed);
	config_lookup_float(&config, "max_pos_ra_speed", &(*settings)->max_pos_ra_speed);
	config_lookup_float(&config, "max_neg_ra_speed", &(*settings)->max_neg_ra_speed);
	config_lookup_float(&config, "max_dec_speed", &(*settings)->max_dec_speed);

	config_lookup_float(&config, "backlash_time_ra", &(*settings)->backlash_time_ra);

	config_lookup_float(&config, "high_speed_distance_threshold", &(*settings)->speed_change_distance);
	config_lookup_float(&config, "consider_arrived_distance", &(*settings)->arcmin_consider_reached);
}

void execute_guiding_pulse(double ra_pulse, double dec_pulse, guide_settings *settings) {
	if (ASI_SUCCESS != ASIPulseGuideOn(0, ra_pulse > 0 ? ASI_GUIDE_EAST : ASI_GUIDE_WEST) ||
			ASI_SUCCESS != ASIPulseGuideOn(0, dec_pulse > 0 ? ASI_GUIDE_NORTH : ASI_GUIDE_SOUTH)) {
		fprintf(stderr, "failed to send guiding order, aborting\n");
		exit(1);
	}
	
	struct timeval start;
	if (gettimeofday(&start, NULL)) {
		perror("gettimeofday");
		goto guiding_err;
	}
	/* TODO: review this function, seems wrong */
	double first_wait_s = fmax(fabs(ra_pulse), fabs(dec_pulse));
	if (first_wait_s != 0.0) {
		if (usleep((unsigned int)(first_wait_s * 1000000.0))) {
			perror("usleep");
			goto guiding_err;
		}

		if (first_wait_s == fabs(ra_pulse)) {
			if (ASI_SUCCESS != ASIPulseGuideOff(0, ra_pulse > 0 ? ASI_GUIDE_EAST : ASI_GUIDE_WEST)) {
				fprintf(stderr, "failed to send guiding order, aborting\n");
				goto guiding_err;
			}
		} else {
			if (ASI_SUCCESS != ASIPulseGuideOff(0, dec_pulse > 0 ? ASI_GUIDE_NORTH : ASI_GUIDE_SOUTH)) {
				fprintf(stderr, "failed to send guiding order, aborting\n");
				goto guiding_err;
			}
		}
	}

	struct timeval mid;
	if (gettimeofday(&mid, NULL)) {
		perror("gettimeofday");
		goto guiding_err;
	}
	unsigned int elapsed_us = (mid.tv_sec - start.tv_sec) * 1000000 +
		((int)mid.tv_usec - (int)start.tv_usec);
	fprintf(stdout, "first guiding order stopped after %g s (requested %g)\n",
			elapsed_us / 1000000.0, first_wait_s);

	double second_wait_s = fmin(fabs(ra_pulse), fabs(dec_pulse));
	unsigned int second_wait_us = (unsigned int)(second_wait_s * 1000000.0);
	if (second_wait_us > (elapsed_us + 1000)) {
		unsigned int remaining = second_wait_us - elapsed_us;
		if (usleep(remaining)) {
			perror("usleep");
			goto guiding_err;
		}
	}

	if (second_wait_s == fabs(ra_pulse)) {
		if (ASI_SUCCESS != ASIPulseGuideOff(0, ra_pulse > 0 ? ASI_GUIDE_EAST : ASI_GUIDE_WEST)) {
			fprintf(stderr, "failed to send guiding order, aborting\n");
			goto guiding_err;
		}
	} else {
		if (ASI_SUCCESS != ASIPulseGuideOff(0, dec_pulse > 0 ? ASI_GUIDE_NORTH : ASI_GUIDE_SOUTH)) {
			fprintf(stderr, "failed to send guiding order, aborting\n");
			goto guiding_err;
		}
	}

	// TODO: maybe it's already done if alpha stopped moving first
	if (current_speed > 1 && ra_pulse < 0)
		usleep((int)(settings->backlash_time_ra * 1000000));
	return;

guiding_err:
	if (ASI_SUCCESS != ASIPulseGuideOff(0, ra_pulse > 0 ? ASI_GUIDE_EAST : ASI_GUIDE_WEST) ||
			ASI_SUCCESS != ASIPulseGuideOff(0, dec_pulse > 0 ? ASI_GUIDE_NORTH : ASI_GUIDE_SOUTH)) {
		fprintf(stderr, "failed to send guiding order, aborting\n");
		exit(1);
	}
}

void compute_pulse_times(struct wcs *distance, double *ra_pulse, double *dec_pulse, guide_settings *settings) {
	double guiding_pulse_ra = distance->alpha / EARTH_DEGREES_PER_SECOND;
	double guiding_pulse_dec = distance->delta / settings->dec_guiding_speed;

	if (fabs(guiding_pulse_ra) > settings->speed_change_distance ||
			fabs(guiding_pulse_dec) > settings->speed_change_distance) {
		if (current_speed <= 1) {
			fputs("long distance to travel, set high speed then press <ENTER>", stdout);
			char buf[3];
			fgets(buf, 3, stdin);
			current_speed = 2;	// any value will do
		}
		// TODO: except during this time, earth will rotate
		*ra_pulse = distance->alpha > 0 ?
			distance->alpha / settings->max_pos_ra_speed :
			distance->alpha / settings->max_neg_ra_speed;
		*dec_pulse = distance->delta / settings->max_dec_speed;
	} else {
		if (current_speed > 1) {
			fputs("short distance to travel, set guiding (lowest) speed then press <ENTER>", stdout);
			char buf[3];
			fgets(buf, 3, stdin);
			current_speed = 1;
		}
		*ra_pulse = guiding_pulse_ra;
		*dec_pulse = guiding_pulse_dec;
	}
}

