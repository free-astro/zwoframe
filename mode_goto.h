#ifndef _MODE_GOTO_H
#define _MODE_GOTO_H

#include "zwoasi.h"

void mode_goto_stop();
void mode_goto_init();
void mode_goto_start(exposure_params _params);

#endif
