#include <stdio.h>
#include <fitsio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include "ASICamera2.h"
#include "fits.h"

//static char* ASI_bayer_patterns[] = {"RGGB", "BGGR", "GRBG", "GBRG"};

static int write_fits_header(fitsfile *fptr, int exposure_ms, int bin, int offset, int gain, ASI_CAMERA_INFO *camera_info, char *start_time, long *temperature) {
	double zero = 32768.0;
	double scale = 1.0;
	int status = 0;
	fits_update_key(fptr, TDOUBLE, "BZERO", &zero,
			"offset data range to that of unsigned short", &status);

	status = 0;
	fits_update_key(fptr, TDOUBLE, "BSCALE", &scale, "default scaling factor",
			&status);

	status = 0;
	fits_update_key(fptr, TSTRING, "ROWORDER", "TOP-DOWN",
			"Order of the rows in image array", &status);

	status = 0;
	fits_update_key(fptr, TSTRING, "INSTRUME", camera_info->Name,
			"instrument name", &status);
	// optionally add FOCALLEN, APERTURE, TELESCOP, OBSERVER

	status = 0;
	fits_update_key(fptr, TSTRING, "DATE-OBS", start_time,
			"UTC time at the begin of acquisition", &status);

	status = 0;
	int itmp;
	char fit_date[40];
	fits_get_system_time(fit_date, &itmp, &status);
	fits_update_key(fptr, TSTRING, "DATE", fit_date,
			"UTC date that FITS file was created", &status);

	status = 0;
	double exp = exposure_ms * 0.001;
	fits_update_key(fptr, TDOUBLE, "EXPTIME", &exp,
			"Exposure time [s]", &status);

	status = 0;
	fits_update_key(fptr, TUSHORT, "OFFSET", &offset,
			"Camera offset", &status);

	status = 0;
	float px_size = camera_info->PixelSize;
	fits_update_key(fptr, TFLOAT, "XPIXSZ", &px_size,
			"X pixel size microns", &status);
	fits_update_key(fptr, TFLOAT, "YPIXSZ", &px_size,
			"Y pixel size microns", &status);

	status = 0;
	fits_update_key(fptr, TUINT, "XBINNING", &bin,
				"Camera binning mode", &status);
	fits_update_key(fptr, TUINT, "YBINNING", &bin,
				"Camera binning mode", &status);

	double t;
	if (temperature)
		t = *temperature * 0.1;
	else {
		long temp;
		ASI_BOOL bAuto = ASI_FALSE;
		ASIGetControlValue(0, ASI_TEMPERATURE, &temp, &bAuto);
		status = 0;
		t = temp * 0.1;
	}
	fits_update_key(fptr, TDOUBLE, "CCD-TEMP", &t,
			"CCD temp in C", &status);

	if (camera_info->IsColorCam) {
		status = 0;
		fits_update_key(fptr, TSTRING, "BAYERPAT",
				ASI_bayer_patterns[camera_info->BayerPattern],
				"Bayer color pattern", &status);
	}

	status = 0;
	double cvf = (double)camera_info->ElecPerADU;
	fits_update_key(fptr, TDOUBLE, "CVF", &cvf,
			"Conversion factor (e-/adu)", &status);

	status = 0;
	fits_update_key(fptr, TUINT, "GAIN", &gain,
			"Camera gain", &status);

	status = 0;
	fits_update_key(fptr, TSTRING, "PROGRAM", "zwoframe",
			"Software that created this HDU", &status);
	return 0;
}

static void report_fits_error(int status) {
	if (status) {
		char errmsg[FLEN_ERRMSG];
		while (fits_read_errmsg(errmsg)) {
			fprintf(stderr, "FITS error: %s\n",
					errmsg[0] != '\0' ? errmsg : "unknown" );
		}
	}
}

// assumed: no ROI, 16 bits
int save_image(const char *filename, unsigned char *buffer, int w, int h,
		int exposure_ms, int bin, int offset, int gain,
		ASI_CAMERA_INFO *camera_info, char *start_time, long *temperature) {
	fitsfile *fptr;

	if (unlink(filename)) { /* Delete old file if it already exists */
		if (errno != ENOENT) {
			perror("Cannot remove FITS file");
			return 1;
		}
	}

	int status = 0;
	if (fits_create_diskfile(&fptr, filename, &status)) {
		report_fits_error(status);
		return 1;
	}

	long naxes[3];
	naxes[0] = w;
	naxes[1] = h;
	naxes[2] = 1;
	if (fits_create_img(fptr, USHORT_IMG, 2, naxes, &status)) {
		fprintf(stderr, "failed to create FITS image\n");
		report_fits_error(status);
		return 1;
	}

	if (write_fits_header(fptr, exposure_ms, bin, offset, gain, camera_info, start_time, temperature)) {
		fprintf(stderr, "failed to write FITS header\n");
		return 1;
	}

	size_t buffer_elements = naxes[0] * naxes[1];
	long orig[3] = { 1L, 1L, 1L };
	if (fits_write_pix(fptr, USHORT_IMG, orig, buffer_elements, buffer, &status)) {
		fprintf(stderr, "failed to write image data:\n");
		report_fits_error(status);
		return 1;
	}

	status = 0;
	fits_close_file(fptr, &status);
	fprintf(stdout, "image written to %s (%d x %d)\n", filename, w, h);
	return status;
}

/* for debug purposes, reading 16 bits ushort FITS */
int read_fits(char *filename, unsigned char **buffer, int *w, int *h, int *is_rgb, ASI_BAYER_PATTERN *pattern) {
	fitsfile *fptr;
	int status = 0;
	fits_open_diskfile(&fptr, filename, READONLY, &status);
	if (status) {
		report_fits_error(status);
		return 1;
	}
	int naxis, bitpix, zero = 0;
	long naxes[3];
	fits_get_img_param(fptr, 3, &bitpix, &naxis, naxes, &status);
	if (status) {
		report_fits_error(status);
		return 1;
	}
	long nbdata = naxes[0] * naxes[1];
	*buffer = malloc(nbdata * 2);
	if (!*buffer) {
		fprintf(stderr, "allocation error in FITS reader\n");
		return 1;
	}
	fits_read_img(fptr, TUSHORT, 1, nbdata, &zero, *buffer, &zero, &status);
	if (status) {
		report_fits_error(status);
		return 1;
	}

	char bayer_pattern[FLEN_VALUE];
	fits_read_key(fptr, TSTRING, "BAYERPAT", &bayer_pattern, NULL, &status);
	*is_rgb = !status;
	if (!status) {
		if (!strncmp(bayer_pattern, "RGGB", 4))
			*pattern = ASI_BAYER_RG;
		else if (!strncmp(bayer_pattern, "BGGR", 4))
			*pattern = ASI_BAYER_BG;
		else if (!strncmp(bayer_pattern, "GBRG", 4))
			*pattern = ASI_BAYER_GB;
		else if (!strncmp(bayer_pattern, "GRBG", 4))
			*pattern = ASI_BAYER_GR;
	}

	*w = naxes[0];
	*h = naxes[1];
	return 0;
}

char *get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	struct tm *utc = gmtime(&tv.tv_sec);
	char *str = malloc(26);
	double seconds = (double)utc->tm_sec + tv.tv_usec / 10000000.0;
	int status = 0;
	fits_time2str(utc->tm_year + 1900, utc->tm_mon + 1, utc->tm_mday, utc->tm_hour, utc->tm_min, seconds, 3, str, &status);
	if (status)
		str[0] = '\0';
	return str;
}

