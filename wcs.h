#ifndef _WCS_H
#define _WCS_H

struct wcs {
	double alpha, delta;
};

struct wcs *wcs_from_ra_dec(double ra_h, double ra_m, double ra_s, double dec_deg, double dec_m, double dec_s);

struct wcs *wcs_from_colon_string(const char *ra, const char *dec);

void wcs_to_triple_colon_string(struct wcs *coords, char **ra, char **dec);

void wcs_to_pretty_print(struct wcs *coords, char **ra, char **dec);

#endif
