/*
 * This file extracted from Siril, an astronomy image processor.
 * Copyright (C) 2005-2011 Francois Meyer (dulle at free.fr)
 * Copyright (C) 2012-2021 team free-astro (see more in AUTHORS file)
 * Reference site is https://free-astro.org/index.php/Siril
 *
 * Siril is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Siril is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Siril. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <ASICamera2.h>
#include <sys/time.h>
#include "peaker.h"

#define MAX_STARS 100000

typedef struct {
	int x, y;
} pointi;

/*
 * Histogram median for very large array of unsigned short
 * (C) Emmanuel Brandt 2019-02
 * @param a array of unsigned short to search
 * @param n size of the array
 * @return median as a double (for n odd)
 * Use temp storage h to build the histogram. Complexity O(2*N)
 */
double histogram_median(WORD *a, size_t n, int mutlithread) {
	const size_t s = sizeof(unsigned int);
	unsigned int *h = (unsigned int*) calloc(USHRT_MAX + 1, s);
	if (!h) {
		fprintf(stderr, "memory allocation error for median computation\n");
		return -1.0;
	}

#ifdef _OPENMP
#pragma omp parallel if (mutlithread)
#endif
	{
		unsigned int *hthr = (unsigned int*) calloc(USHRT_MAX + 1, s);
		if (!hthr) {
			fprintf(stderr, "memory allocation error for median computation\n");
		}
		else {
#ifdef _OPENMP
#pragma omp for nowait
#endif
			for (size_t i = 0; i < n; i++) {
				hthr[a[i]]++;
			}
#ifdef _OPENMP
#pragma omp critical
#endif
			{
				// add per thread histogram to main histogram
#ifdef _OPENMP
#pragma omp simd
#endif
				for (int ii = 0; ii <= USHRT_MAX; ++ii) {
					h[ii] += hthr[ii];
				}
			}
			free(hthr);
		}
	}
	unsigned int i= 0, j = 0, k = n / 2;

	unsigned int sum = 0;
	if (n % 2 == 0) {
		for (; sum <= k - 1; j++)
			sum += h[j];
		i = j;
	}

	for (; sum <= k; i++)
		sum += h[i];

	free(h);
	return (n % 2 == 0) ? (double) (i + j - 2) / 2.0 : (double) (i - 1);
}

// warning: in place computation
double quickmedian(WORD *a, size_t n) {
        WORD pivot, tmp;
        size_t k = n / 2;       // size to sort
        size_t left = 0;        // left index
        size_t right = n - 1;   // right index

        while (left < right) { //we stop when our indicies have crossed
                size_t pindex = (left + right) / 2; // pivot selection, this can be whatever
                pivot = a[pindex];
                a[pindex] = a[right];
                a[right] = pivot; // SWAP(pivot,right)

                for (size_t i = pindex = left; i < right; i++) {
                        if (a[i] < pivot) { // SWAP
                                tmp = a[pindex];
                                a[pindex] = a[i];
                                a[i] = tmp;
                                pindex++;
                        }
                }
                a[right] = a[pindex];
                a[pindex] = pivot; // SWAP

                if (pindex < k)
                        left = pindex + 1;
                else
                        // pindex >= k
                        right = pindex;
        }
        return (n % 2 == 0) ?
                        ((double) a[k - 1] + (double) a[k]) / 2.0 : (double) a[k];
}

// warning: in place computation
double quickmedian_float(float *a, size_t n) {
	fprintf(stdout, "starting quickmedian_float\n");
        // Use faster and robust sorting network for small size array
        size_t k = n / 2;       // size to sort
        size_t left = 0;        // left index
        size_t right = n - 1;   // right index
        float tmp;

        while (left < right) { //we stop when our indicies have crossed
                size_t pindex = (left + right) / 2; // pivot selection, this can be whatever
                float pivot = a[pindex];
                a[pindex] = a[right];
                a[right] = pivot; // SWAP(pivot,right)

                for (size_t i = pindex = left; i < right; i++) {
                        if (a[i] < pivot) { // SWAP
                                tmp = a[pindex];
                                a[pindex] = a[i];
                                a[i] = tmp;
                                pindex++;
                        }
                }
                a[right] = a[pindex];
                a[pindex] = pivot; // SWAP(right,j)

                if (pindex < k)
                        left = pindex + 1;
                else
                        // pindex >= k
                        right = pindex;
        }
	fprintf(stdout, "end of quickmedian_float\n");
        return (n % 2 == 0) ? ((double) a[k - 1] + a[k]) / 2.0 : (double) a[k];
}

double compute_sigma(WORD *a, size_t n) {
        double sum = 0., sum2 = 0.;
	for (size_t i = 0; i < n; i++) {
		double xtemp = (double)a[i];
		sum += xtemp;
		sum2 += (xtemp * xtemp);
	}
	double mean = sum / n;
	return sqrt((sum2 / n) - (mean * mean));
}

static int is_star(fitted_PSF *result, double radius, double roundness_threshold) {
        if (isnan(result->fwhmx) || isnan(result->fwhmy))
                return 0;
        if (isnan(result->x0) || isnan(result->y0))
                return 0;
        if (isnan(result->mag))
                return 0;
        if ((result->x0 <= 0.0) || (result->y0 <= 0.0))
                return 0;
        if (result->sx > 3.0 * radius || result->sy > 3.0 * radius)
                return 0;
        if (result->fwhmx <= 0.0 || result->fwhmy <= 0.0)
                return 0;
        if ((result->fwhmy / result->fwhmx) < roundness_threshold)
                return 0;
        return 1;
}

static int compare_stars(const void* star1, const void* star2) {
	fitted_PSF *s1 = *(fitted_PSF**)star1;
	fitted_PSF *s2 = *(fitted_PSF**)star2;
	if (s1->mag < s2->mag)
		return -1;
	else if (s1->mag > s2->mag)
		return 1;
	else return 0;
}

static void sort_stars(fitted_PSF **stars, int total) {
        if (*(&stars))
                qsort(*(&stars), total, sizeof(fitted_PSF*), compare_stars);
}

static int minimize_candidates(WORD *data, int rx, int ry, int radius, double roundness_threshold,
		double bg, pointi *candidates, int nb_candidates, fitted_PSF ***retval) {
        gsl_matrix *z = gsl_matrix_alloc(radius * 2, radius * 2);
        int nbstars = 0;

	WORD **image_ushort = malloc(ry * sizeof(WORD *));
	for (int k = 0; k < ry; k++)
		image_ushort[k] = data + k * rx;

        fitted_PSF **results = malloc((nb_candidates + 1) * sizeof(fitted_PSF *));
        if (!results) {
                return 0;
        }

        for (int candidate = 0; candidate < nb_candidates; candidate++) {
                int x = candidates[candidate].x, y = candidates[candidate].y;
                int ii, jj, i, j;
                /* FILL z */
		for (jj = 0, j = y - radius; j < y + radius; j++, jj++) {
			for (ii = 0, i = x - radius; i < x + radius;
					i++, ii++) {
				gsl_matrix_set(z, ii, jj, (double)image_ushort[j][i]);
			}
		}

                fitted_PSF *cur_star = psf_global_minimisation(z, bg, 0, 0, 0);
                if (cur_star) {
			/*fprintf(stdout, "PSF fit Result:\n"
					"x0=%0.2f px, y0=%0.2f px\n"
					"FWHM X=%0.2f%s, FWHM Y=%0.2f%s\n"
					"Angle=%0.2f deg\n"
					"Background value=%0.6f\n"
					"Maximal intensity=%0.6f\n"
					"Magnitude=%0.2f\n"
					"RMSE=%.3e\n", cur_star->x0, cur_star->y0,
					cur_star->fwhmx, cur_star->units, cur_star->fwhmy, cur_star->units,
					cur_star->angle, cur_star->B, cur_star->A, cur_star->mag, cur_star->rmse);*/

                        if (is_star(cur_star, radius, roundness_threshold)) {
                                cur_star->layer = -1;
                                cur_star->xpos = (x - radius) + cur_star->x0 - 1.0;
                                cur_star->ypos = (y - radius) + cur_star->y0 - 1.0;
                                results[nbstars] = cur_star;
				//fprintf(stdout, "%03d: %8f,%8f %f mag %f\n",
				//		nbstars, cur_star->xpos, cur_star->ypos,
				//		cur_star->fwhmx, cur_star->mag);
				nbstars++;
			}
			else free(cur_star);
		}
	}

        results[nbstars] = NULL;
        if (retval)
                *retval = results;
        gsl_matrix_free(z);
        free(image_ushort);
        return nbstars;
}

/* computes a list of minimized stars in the passed monochrome image */
fitted_PSF **peaker(WORD *data, int rx, int ry, int radius, double ksigma, double roundness_threshold) {
	int nbstars = 0;
	double bg = histogram_median(data, rx * ry, 1);
	double sigma = compute_sigma(data, rx * ry);
	float norm = 65535.0f;
	float threshold = (float)(bg + ksigma * sigma);
	fprintf(stdout, "median (bg): %4g, sigma: %5g, threshold with %g sigma: %g, radius = %d\n", bg, sigma, ksigma, threshold, radius);

	pointi *candidates = malloc(MAX_STARS * sizeof(pointi));
	float *filtered = malloc(rx * ry * sizeof(float));
	if (!filtered || !candidates) {
		fprintf(stderr, "could not allocate memory for demosaicing\n");
		exit(1);
	}
	fprintf(stdout, "median filtering image (%d x %d)...\n", rx, ry);
	int above_thresh = 0;
	for (int y = 1; y < ry - 1; y++) {
		int pix_idx = y * rx + 1;
		for (int x = 1; x < rx - 1; x++) {
			/* median 3x3 */
			WORD local[9] = {
				data[(y - 1) * rx + x - 1],
				data[(y - 1) * rx + x],
				data[(y - 1) * rx + x + 1],
				data[y * rx + x - 1],
				data[y * rx + x],
				data[y * rx + x + 1],
				data[(y + 1) * rx + x - 1],
				data[(y + 1) * rx + x],
				data[(y + 1) * rx + x + 1]
			};
			filtered[pix_idx] = (float)quickmedian(local, 9);
			if (filtered[pix_idx] > threshold && filtered[pix_idx] < norm)
				above_thresh++;
			pix_idx++;
		}
	}

	fprintf(stdout, "searching for star candidates (%d above threshold)...\n", above_thresh);
	/* Search for candidate stars in the filtered image */
	for (int y = radius + 1; y < ry - radius - 1; y++) {
		for (int x = radius + 1; x < rx - radius - 1; x++) {
			float pixel = filtered[y * rx + x];
			if (pixel > threshold && pixel < norm) {
				int bingo = 1;
				float neighbor;
				for (int yy = y - 1; yy <= y + 1; yy++) {
					for (int xx = x - 1; xx <= x + 1; xx++) {
						if (xx == x && yy == y)
							continue;
						neighbor = filtered[yy * rx + xx];
						if (neighbor > pixel) {
							bingo = 0;
							//fprintf(stdout, "%4d,%4d [%5g] but %4d,%4d [%5g]\n", x, y, pixel, xx, yy, neighbor);
							break;
						} else if (neighbor == pixel) {
							if ((xx <= x && yy <= y) || (xx > x && yy < y)) {
								//fprintf(stdout, "%4d,%4d [%5g] but %4d,%4d [%5g]\n", x, y, pixel, xx, yy, neighbor);
								bingo = 0;
								break;
							}
						}
					}
					if (!bingo) break;
				}
				if (bingo && nbstars < MAX_STARS) {
					//fprintf(stdout, "%4d,%4d [%5g] ok!\n", x, y, pixel);
					candidates[nbstars].x = x;
					candidates[nbstars].y = y;
					nbstars++;
					if (nbstars == MAX_STARS) break;
				}
			}
		}
		if (nbstars == MAX_STARS) break;
	}
	fprintf(stdout, "peaker found %d star candidates\n", nbstars);

        fitted_PSF **results;
        nbstars = minimize_candidates(data, rx, ry, radius, roundness_threshold, bg, candidates, nbstars, &results);
        if (nbstars == 0) {
		fprintf(stdout, "no star found in image\n");
                results = NULL;
	} else {
		fprintf(stdout, "%d stars found\n", nbstars);
		sort_stars(results, nbstars);
	}
	free(filtered);
	free(candidates);
	return results;
}

/* reject outliers based on the median FWHM and keep only brightest stars */
void filter_stars(fitted_PSF **stars, double min_fwhm, double magnitude_from_brightest) {
	if (!stars) return;
	float *fwhmx = malloc(MAX_STARS * sizeof(float));
	if (!fwhmx) {
		fprintf(stderr, "could not allocate FWHM array of size %d\n", MAX_STARS);
		exit(1);
	}

	double lowest_mag = 1000000.0;
	int size, i;
	for (i = 0; stars[i]; i++) {
		fwhmx[i] = stars[i]->fwhmx;
		if (stars[i]->mag < lowest_mag)
			lowest_mag = stars[i]->mag;
	}
	size = i;

	double fwhm_threshold = fmax(min_fwhm, quickmedian_float(fwhmx, size) * 0.5);

	int kept = 0;
	for (i = 0; i < size; i++) {
		if (stars[i]->mag > magnitude_from_brightest + lowest_mag ||
				stars[i]->fwhmx < fwhm_threshold) {
			free(stars[i]);
			continue;
		}
		if (i != kept)
			stars[kept] = stars[i];
		kept++;
	}
	stars[kept] = NULL;
}

void free_fitted_stars(fitted_PSF **stars) {
        int i = 0;
        while (stars && stars[i])
                free(stars[i++]);
        free(stars);
}

