/* this is a focus helper: it takes a shot, finds some stars near the centre,
 * then activates a video feed on the region containing them and displays the
 * FWHM for each frame
 *
 * The star detection, image statistics and PSF fitting code comes from siril,
 * so the license for this particular program is GPLv3.
 *
 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ASICamera2.h>
#include "peaker.h"
#include "PSF.h"
#include "zwoasi.h"

#define DEBUG_PEAKER

#ifndef DEBUG_PEAKER
/* camera settings */
static exposure_params params = {
	.gain = 300,
	.exposure_ms = 1000,
	.bin = 1,
	// binning is used as 1 in this program, but for color cameras,
	// demosaicing is done with averaging the two green pixels of the Bayer
	// square, so the result in image size is like a bin2
	.offset = 8 // [0, 100]
};
#endif

/* star detection parameters (same as siril) */
int radius = 40;	// default is 10, increase for high resolution
double ksigma = 3.0;	// only the brightest
double roundness_threshold = 0.20; // changing the focus will make the mirror or telescope move

/* star filtering settings */
double magnitude_threshold_from_brightest = 5.0; // keep only stars in the 5 first relative magnitudes
double minimum_plausible_fwhm = 6.0;	// below this value, it is probably a bright pixel or noise
int min_number_of_stars = 3;	// find the smallest area that contains as many bright stars

#ifndef DEBUG_PEAKER
static void parse_args(int argc, char **argv);
#endif

void find_and_set_ROI(fitted_PSF **stars, image *image, int *new_rx, int *new_ry) {
	int rx = image->rx, ry = image->ry;
	int bin1 = 0, bin2 = 0, bin3 = 0, bin4 = 0, bin5 = 0;
	int bin2_x1 = rx / 4, bin2_x2 = 3 * rx / 4;
	int bin2_y1 = ry / 4, bin2_y2 = 3 * ry / 4;
	int bin3_x1 = rx / 3, bin3_x2 = 2 * rx / 3;
	int bin3_y1 = ry / 3, bin3_y2 = 2 * ry / 3;
	int bin4_x1 = 3 * rx / 8, bin4_x2 = 5 * rx / 8;
	int bin4_y1 = 3 * ry / 8, bin4_y2 = 5 * ry / 8;
	int bin5_x1 = 2 * rx / 5, bin5_x2 = 3 * rx / 5;
	int bin5_y1 = 2 * ry / 5, bin5_y2 = 3 * ry / 5;

	filter_stars(stars, minimum_plausible_fwhm, magnitude_threshold_from_brightest);
	int i = 0;
	if (stars)
		while (stars[i]) {
			fitted_PSF *star = stars[i];
			if (star->xpos > bin5_x1 && star->xpos < bin5_x2 && star->ypos > bin5_y1 && star->ypos < bin5_y2)
				bin5++;
			else if (star->xpos > bin4_x1 && star->xpos < bin4_x2 && star->ypos > bin4_y1 && star->ypos < bin4_y2)
				bin4++;
			else if (star->xpos > bin3_x1 && star->xpos < bin3_x2 && star->ypos > bin3_y1 && star->ypos < bin3_y2)
				bin3++;
			else if (star->xpos > bin2_x1 && star->xpos < bin2_x2 && star->ypos > bin2_y1 && star->ypos < bin2_y2)
				bin2++;
			else bin1++;
			i++;
		}

	fprintf(stdout, "  found %d stars (%d in 1/5 of the image, %d more in 1/4, %d more in 1/3, %d more in 1/2, and %d in the rest)\n",
			i, bin5, bin4, bin3, bin2, bin1);
	if (i < min_number_of_stars) {
		fprintf(stdout, "did not find the configured minimum number of stars, using the centermost one.\n");
		min_number_of_stars = 1;
	}

	int w, h;
#ifndef DEBUG_PEAKER
	rx = image->camera_info.MaxWidth, ry = image->camera_info.MaxHeight;
#endif
	if (bin5 >= min_number_of_stars) {
		w = rx / 5; h = ry / 5;
	} else if (bin5 + bin4 >= min_number_of_stars) {
		w = rx / 4; h = ry / 4;
	} else if (bin5 + bin4 + bin3 >= min_number_of_stars) {
		w = rx / 3; h = ry / 3;
	} else if (bin5 + bin4 + bin3 + bin2 >= min_number_of_stars) {
		w = rx / 2; h = ry / 2;
	} else	w = rx, h = ry;
	if (image->camera_info.IsColorCam) {
		w = (w/2)*2;
		h = (h/2)*2;
	}

#ifndef DEBUG_PEAKER
	// make a centred region of interest for the next capture
	if (ASI_SUCCESS != ASISetROIFormat(0, w, h, 1, ASI_IMG_RAW16)) {
		fprintf(stderr, "failed to set image format (%d x %d, bin1, 16 bits)\n", w, h);
		ASICloseCamera(0);
		exit(1);
	}
#endif
	*new_rx = w;
	*new_ry = h;
	fprintf(stdout, "ROI set to %d x %d\n", w, h);
}

int main(int argc, char **argv) {
#ifdef DEBUG_PEAKER
	int read_fits(char *filename, unsigned char **buffer, int *w, int *h, int *is_rgb, ASI_BAYER_PATTERN *pattern);

	WORD *buf;
	int rx, ry, is_rgb;
	ASI_BAYER_PATTERN pattern;
	if (argc == 1) {
		fprintf(stderr, "Usage: %s sample.fit\n", argv[0]);
		exit(1);
	}
	if (read_fits(argv[1], (unsigned char**)&buf, &rx, &ry, &is_rgb, &pattern))
		exit(1);
	//fprintf(stdout, "first matrix:\n%5d %5d\n%5d %5d\n", buf[0], buf[1], buf[rx], buf[rx+1]);
	image image;
	image.camera_info.IsColorCam = is_rgb;
	image.camera_info.BayerPattern = pattern;
	data_to_image_struct(&image, buf, 1, rx, ry, -10000, NULL, &image.camera_info);
	//fprintf(stdout, "first pixel: %5d\n", image.data[0]);
	/* find stars and set ROI */
	fitted_PSF **stars = peaker(image.data, image.rx, image.ry,
			radius, ksigma, roundness_threshold);
	int nx, ny;
	find_and_set_ROI(stars, &image, &nx, &ny);
	return 0;
#else
	parse_args(argc, argv);
	/* capture first full-frame image */
	open_camera(0);
	image image;
	capture_frame(params, NULL, &image);
	fprintf(stdout, "initial exposure done, searching for stars\n");

	/* find stars and set ROI */
	fitted_PSF **stars = peaker(image.data, image.rx, image.ry,
			radius, ksigma, roundness_threshold);
	int nx, ny;
	find_and_set_ROI(stars, &image, &nx, &ny);
	free_fitted_stars(stars);
	free(image.data);

	/* open a video stream */
	start_video_capture(NULL, NULL, NULL, NULL);
	long imsize = nx * ny * 2;
	WORD *buffer = malloc(imsize);
	if (!buffer) {
		fprintf(stderr, "could not allocate streaming image buffer\n");
		exit(1);
	}
	fputc('\n', stdout);

	double min_fwhm = 10000.0;
	while (1) {
		/* get a frame and compute FWHM */
		if (get_video_frame(&image, buffer, nx, ny, params.exposure_ms))
			break;
		/* if exposure is short and star detection is long, this will cause latency
		 * and errors the documentation says threads should be used instead
		 */
		stars = peaker(image.data, image.rx, image.ry, radius, ksigma, roundness_threshold);
		filter_stars(stars, minimum_plausible_fwhm, magnitude_threshold_from_brightest);

		if (stars && stars[0]) {
			/* display results */
			int i;
			double fwhm_sum = 0.0, roundness_sum = 0.0;
			for (i = 0; stars[i]; i++) {
				fwhm_sum += stars[i]->fwhmx;
				roundness_sum += stars[i]->fwhmy / stars[i]->fwhmx;
			}
			double fwhm = fwhm_sum/i;
			fprintf(stdout, "\033[A\33[2KT\rFWHM: %3g (min %3g)\troundness: %2g (%d stars)%s\n", fwhm, min_fwhm,
					roundness_sum/i, i, fwhm < min_fwhm ? " NEW MINIMUM!" : "");
			if (fwhm < min_fwhm)
				min_fwhm = fwhm;
		}

		if (image.data != buffer)	// green extraction
			free(image.data);
	}

	stop_video_capture();;
	close_camera();
	return 0;
#endif
}

#ifndef DEBUG_PEAKER
static void show_usage(char *program_name) {
	fprintf(stderr, "Usage: %s [-e exposure_in_milliseconds] [-g gain]\n", program_name);
	exit(1);
}

static void parse_args(int argc, char **argv) {
	int c, tmp;
	opterr = 0;
	while ((c = getopt (argc, argv, "e:g:")) != -1)
		switch (c)
		{
			case 'e':
				tmp = atoi(optarg);
				if (tmp > 0)
					params.exposure_ms = tmp;
				break;
			case 'g':
				tmp = atoi(optarg);
				if (tmp > 0)
					params.gain = tmp;
				break;
			default:
				show_usage(*argv);
		}
}
#endif
