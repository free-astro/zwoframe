#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "wcs.h"

struct wcs *wcs_from_ra_dec(double ra_h, double ra_m, double ra_s, double dec_deg, double dec_m, double dec_s) {
        struct wcs *retval = malloc(sizeof(struct wcs));
        retval->alpha = ra_h * 15.0 + ra_m * 15.0 / 60.0 + ra_s * 15.0 / 3600.0;
        if (dec_deg > 0) {
                retval->delta = ((dec_s / 3600.0) + (dec_m / 60.0) + dec_deg);
        } else {
                retval->delta = (-(dec_s / 3600.0) - (dec_m / 60.0) + dec_deg);
        }
        return retval;
}

struct wcs *wcs_from_colon_string(const char *ra, const char *dec) {
	int ra_h, ra_m, dec_deg, dec_m;
	double ra_s, dec_s;
	if (sscanf(ra, "%d:%d:%lg", &ra_h, &ra_m, &ra_s) != 3) {
		fprintf(stderr, "failed to parse right ascension: %s\n", ra);
		return NULL;
	}
	if (sscanf(dec, "%d:%d:%lg", &dec_deg, &dec_m, &dec_s) != 3) {
		fprintf(stderr, "failed to parse declination: %s\n", dec);
		return NULL;
	}

	return wcs_from_ra_dec(ra_h, ra_m, ra_s, dec_deg, dec_m, dec_s);
}

static void wcs_get_ra_hour_min_sec(double ra, int *hour, int *min, double *sec) {
        int h = (int)(ra / 15.0);
        int m = (int)(((ra / 15.0) - h) * 60.0);
        double s = ((((ra / 15.0) - h) * 60.0) - m) * 60.0;
	if (hour) *hour = h;
	if (min) *min = m;
	if (sec) *sec = s;
}

static void wcs_get_dec_deg_min_sec(double dec, int *deg, int *min, double *sec) {
        int d = (int)dec;
	int m = abs((int)((dec - d) * 60.0));
        double s = (fabs((dec - d) * 60.0) - m) * 60.0;
	if (deg) *deg = d;
	if (min) *min = m;
	if (sec) *sec = s;
}

void wcs_to_triple_colon_string(struct wcs *coords, char **ra, char **dec) {
	int ra_h, ra_m, dec_deg, dec_m;
	double ra_s, dec_s;
	*ra = malloc(20);
	*dec = malloc(20);
	wcs_get_ra_hour_min_sec(coords->alpha, &ra_h, &ra_m, &ra_s);
	wcs_get_dec_deg_min_sec(coords->delta, &dec_deg, &dec_m, &dec_s);
	snprintf(*ra, 20, "%02d:%02d:%02.3f", ra_h, ra_m, ra_s);
	snprintf(*dec, 20, "%02d:%02d:%02.3f", dec_deg, dec_m, dec_s);
}

void wcs_to_pretty_print(struct wcs *coords, char **ra, char **dec) {
	int ra_h, ra_m, dec_deg, dec_m;
	double ra_s, dec_s;
	*ra = malloc(24);
	*dec = malloc(24);
	wcs_get_ra_hour_min_sec(coords->alpha, &ra_h, &ra_m, &ra_s);
	wcs_get_dec_deg_min_sec(coords->delta, &dec_deg, &dec_m, &dec_s);
	snprintf(*ra, 24, "%02dh%02d'%02.3f\"", ra_h, ra_m, ra_s);
	snprintf(*dec, 24, "%02d°%02d'%02.3f\"", dec_deg, dec_m, dec_s);
}
