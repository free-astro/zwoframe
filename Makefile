# this makefile assumes the program is complied for the machine on which it is executed
# the ZWO ASI library must be installed on the system
# use the original makefile given in the SDK to cross compile
ver = release
CC = gcc

CFITS_INC=`pkg-config --cflags cfitsio`
CFITS_LIB=`pkg-config --libs cfitsio`
GSL_INC=`pkg-config --cflags gsl`
GSL_LIB=`pkg-config --libs gsl`
CONFIG_INC=`pkg-config --cflags libconfig`
CONFIG_LIB=`pkg-config --libs libconfig`
GTK_INC=`pkg-config --cflags gtk+-3.0`
GTK_LIB=`pkg-config --libs gtk+-3.0` `pkg-config --libs gmodule-2.0`

ifeq ($(ver), debug)
CFLAGS = -Wall -g -march=native -DGLIBC_20
else
CFLAGS = -Wall -Wno-unused-result -g -O3 -march=native -DGLIBC_20
endif

all: tablet zwoframe guidetotarget focusaid gui_tests/overlay gui_tests/stackoverlay

tablet: tablet.c tablet.h mode_focus.c mode_focus.h mode_goto.c mode_goto.h wcs.h wcs.c zwoasi.c zwoasi.h fits.c PSF.c PSF.h peaker.c peaker.h image_display.c image_display.h plate_solve.c plate_solve.h guide.c guide.h
	$(CC) $(CFLAGS) $(GTK_INC) $(GSL_INC) $(CONFIG_INC) tablet.c mode_focus.c mode_goto.c wcs.c plate_solve.c guide.c image_display.c zwoasi.c PSF.c peaker.c fits.c -o tablet -lASICamera2 $(GSL_LIB) $(GTK_LIB) $(CFITS_LIB) $(CONFIG_LIB) -lm

gui_tests/overlay: gui_tests/overlay.c
	$(CC) $(CFLAGS) $(GTK_INC) gui_tests/overlay.c -o gui_tests/overlay $(GTK_LIB) -lm

gui_tests/stackoverlay: gui_tests/stackoverlay.c
	$(CC) $(CFLAGS) $(GTK_INC) gui_tests/stackoverlay.c -o gui_tests/stackoverlay $(GTK_LIB) -lm

zwoframe: zwoframe.c zwoasi.c fits.c
	$(CC) $(CFLAGS) $(CFITS_INC) zwoframe.c zwoasi.c fits.c -o zwoframe -lASICamera2 $(CFITS_LIB)

guidetotarget: guidetotarget.c zwoasi.c zwoasi.h fits.c wcs.h wcs.c plate_solve.h plate_solve.c guide.h guide.c
	$(CC) $(CFLAGS) $(CFITS_INC) $(CONFIG_INC) guidetotarget.c -o guidetotarget wcs.c plate_solve.c guide.c zwoasi.c fits.c -lASICamera2 -lm $(CONFIG_LIB) $(CFITS_LIB)

focusaid: focusaid.c PSF.c PSF.h peaker.c peaker.h zwoasi.c zwoasi.h fits.c
	$(CC) $(CFLAGS) $(CFITS_INC) $(GSL_INC) -o focusaid PSF.c peaker.c focusaid.c zwoasi.c fits.c -lASICamera2 -lm $(GSL_LIB) $(CFITS_LIB)

clean:
	rm -f tablet zwoframe guidetotarget focusaid

