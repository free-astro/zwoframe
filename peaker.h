#ifndef _PEAKER_H_
#define _PEAKER_H_

#include <ASICamera2.h>
#include "PSF.h"
#include "zwoasi.h"

fitted_PSF **peaker(WORD *data, int rx, int ry, int radius, double ksigma, double roundness_threshold);
void filter_stars(fitted_PSF **stars, double min_fwhm, double magnitude_from_brightest);
void free_fitted_stars(fitted_PSF **stars);

#endif
